import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.util.*;
/**
 * Driver Class to actually run TTR
 * @author Team Snuffles
 * @version 1.0
 */
public class Driver
{
    static Map map; //static instance variable of map
    static CardDeck deck; //static instance variable of the cards
    static LeftPanel left; //static instance variable of the left panel
    static RightPanel right; //static instance variable of the right panel
    static LinkedListOfCities nodes; //static instance variable of the cities
    static Player[] players; //static instance variable of the players
    /**
     *Main method to actually execute the program
     *@param args commandline arguments
     */
    public static void main(String args[])
    {
        System.gc(); //garbage cleaner to avoid memory leaks
        Image icon=null;
        try{//read in program icon
            icon=ImageIO.read(new File
                (".\\Ticket to Ride Photos\\icon.jpg"));
        }
        catch(Exception e){//catch file not found 
            System.err.print("File Not Found");
        }
        JFrame opening = new JFrame("Ticket To Ride");//opening JFrame
        GamePanel game= new GamePanel(); //start screen
        game.init();//initialize the init of start screen
        opening.add(game, BorderLayout.CENTER); //add layout to the game
        Dimension DimMax = //set dimension to max
            Toolkit.getDefaultToolkit().getScreenSize(); 

        opening.setExtendedState(JFrame.MAXIMIZED_BOTH);//maxscreen
        opening.pack();
        opening.setIconImage(icon);//set program icon
        opening.setVisible(true);

        //to select amount of players
        Object[] config = {"Two", "Three", "Four"};
        String[] color = new String[] { //to select color of player
                "RED", "GREEN", "BLUE", "YELLOW", "BLACK"};
        //joptionpane for player select
        int n = JOptionPane.showOptionDialog(null, 
                "How many Tickets will you be purchasing today?",
                "Player Select",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                config, //array
                config[0]);
        Color[] c=new Color[]{//array of colors
                Color.RED,Color.GREEN,Color.BLUE,Color.YELLOW};
        JFrame frame = new JFrame();
        JFrame program=new JFrame("Ticket To Ride");
        program.addWindowListener(
            //anon class to clean memory leaks
            new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    System.gc();
                    System.exit(0);
                }
            });

        deck = new CardDeck(n);//fills deck by amount of players
        n+=2;//increments n by two 
        players=new Player[n]; //fills players by n
        boolean gameOver=false;//count to keep track of game
        for(int i=0;i<players.length;i++){//loop to get players names
            String name=JOptionPane.showInputDialog(//prompt for name
                    frame, "Enter Player " + (i+1) + "'s name:");
            if(name==null||name.equals("")){//default names
                switch(i){
                    case 0:name="Lim'sLimbs";
                    break;
                    case 1:name="BriemTime";
                    break;
                    case 2:name="Good#2Know";
                    break;
                    case 3:name="Sir Snuffles";
                    break;
                }
            }
            if(name.length()>12)//cuts off name if too long
                name=name.substring(0,12);
            //fill player array with info
            players[i]=new Player(i,c[i],name);
            //start selection of destination cards
            ArrayList<Destination> selectable=
                Driver.deck.destStartSelection(); 
            //images of start selection
            ArrayList<ImageIcon> selectableImage= Driver.deck.toImage(
                    selectable.toArray(new Destination[selectable.size()]));

            boolean hitCancel=false;
            JFrame choose=new JFrame();

            //loop to allow players to select destination card
            //keeps going until you pick at least three
            while( players[i].getDestinationCard().size()<3||!hitCancel){
                hitCancel=false;
                ImageIcon[] arr=new ImageIcon[selectable.size()];
                arr=selectableImage.toArray(arr);

                int index= JOptionPane.showOptionDialog(frame,
                        "How many Tickets will you be purchasing today?",
                        "Destination Select",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        arr, //array
                        arr[0]);
                if(index==arr.length-1){//stop when there are none left
                    hitCancel=true;
                }else if(index==-1){continue;}
                else{
                    //actually gives player the card
                    players[i].addDestinationCard(selectable.get(index));
                    selectable.remove(index);//removes the card
                    selectableImage.remove(index);//removes the image
                    if(players[i].getDestinationCard().size()==5){
                        break;//breaks if there are more than 5 in hand
                    }
                }
            }
            if(name.equals("Lim God")||name.equals("l")){//cheat code
                //gives player more train and tech cards
                players[i].headStart();
            }
        }       
        if(n==1){
            //allows you to play by yourself, but you get taunted first
            JOptionPane.showMessageDialog(frame,
                "You Are Playing Alone? How Sad!", 
                "Loser Has No Friends", 
                JOptionPane.INFORMATION_MESSAGE);
        }
        for (int i = 0; i < n; i++){
            // select color for each player

            String colorPic = (String)JOptionPane.showInputDialog(null,
                    "Select a Color: Player " + (i+1) +", " + 
                    players[i].getName(),
                    "Color Select",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    color,
                    color[0]);
            Color playerColor = null;

            if(colorPic==null){//sets color to null
                players[i].setColor(null);
            }
            //else ifs do the same thing for different colors

            else if (colorPic.equals("RED")){//for red           
                playerColor = Color.RED;
                players[i].setColor(Color.RED);//sets color
                ArrayList<String> colorList = new ArrayList<>(
                        Arrays.asList(color));
                colorList.remove("RED");//removes it form list
                color = colorList.toArray(new String[colorList.size()]);
            }else if (colorPic.equals("GREEN")){           
                playerColor = Color.GREEN;
                players[i].setColor(Color.GREEN);
                ArrayList<String> colorList = new ArrayList<>(
                        Arrays.asList(color));
                colorList.remove("GREEN");
                color = colorList.toArray(new String[colorList.size()]);
            }else if (colorPic.equals("BLUE")){           
                playerColor = Color.BLUE;
                players[i].setColor(Color.BLUE);
                ArrayList<String> colorList = new ArrayList<>(
                        Arrays.asList(color));
                colorList.remove("BLUE");
                color = colorList.toArray(new String[colorList.size()]);
            }else if (colorPic.equals("YELLOW")){           
                playerColor = Color.YELLOW;
                players[i].setColor(Color.YELLOW);
                ArrayList<String> colorList = new ArrayList<>(
                        Arrays.asList(color));
                colorList.remove("YELLOW");
                color = colorList.toArray(new String[colorList.size()]);
            }else if (colorPic.equals("BLACK")){           
                playerColor = Color.BLACK;
                players[i].setColor(Color.BLACK);
                ArrayList<String> colorList = new ArrayList<>(
                        Arrays.asList(color));
                colorList.remove("BLACK");
                color = colorList.toArray(new String[colorList.size()]);
            }
        }

        map = new Map(); 
        left = new LeftPanel();
        right = new RightPanel();
        nodes = new LinkedListOfCities();

        left.setBounds(350,25,200,600); //sets bounds for leftside
        //sets preferred size dimensions for left and right panel
        left.setPreferredSize(new Dimension(350, program.getHeight()));
        right.setPreferredSize(new Dimension(598-264, program.getHeight()));

        program.add(map, BorderLayout.CENTER);
        program.add(left, BorderLayout.WEST);
        program.add(right, BorderLayout.EAST);

        DimMax = Toolkit.getDefaultToolkit().getScreenSize();
        program.setExtendedState(JFrame.MAXIMIZED_BOTH);
        program.pack();
        program.setIconImage(icon);
        opening.setVisible(false);
        program.setVisible(true);
        int lastPlayer = 0;

        while(!gameOver){//actual logic driver
            for(Player p : players){//loops thru players
                p.setIsTurn();//set turn
                if (p.getPiecesLeft() <= 2) {
                    //ending logic 
                    //when someone has less than 2 trains left
                    gameOver = true;
                    lastPlayer = p.getPlayerNumber() - 1;
                }
                while(p.getIsTurn()){
                    //update panels based on actions
                    left.update(p);
                    right.update(p);
                    map.update(p);
                }
            }
        }
        int maxPoints=0;
        int pl=-1;
        for(int i= 0; i <= lastPlayer; i++){//last turn logic
            players[i].setIsTurn();
            while(players[i].getIsTurn()){
                left.update(players[i]);
                right.update(players[i]);
                map.update(players[i]);
            }
            if(players[i].getPoints()>maxPoints){
                maxPoints=players[i].getPoints();
                pl=i;
            }
        }
        program.setVisible(false);
        game.endGame();//ends game
        opening.setVisible(true);
        JOptionPane.showMessageDialog(frame,"Player "+ (pl+1)+" : "+
            players[pl].getName()+" Won!"+" With"+ players[pl].getPoints()
            +" Points");
    }
}