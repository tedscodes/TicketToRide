import javax.swing.JPanel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.io.*;
import javax.imageio.*;
/**
 * Enum of all the destination cards in Ticket to Ride
 * 
 * @author Team Snuffles 
 * @version 1.0
 */
public enum Destination
{
   
    ABERDEEN_GLASFOW(5,"Abereen to glasgow",
    Locations.ABERDEEN, Locations.GLASGOW, null ),

    ABERYSTWYTH_CARDIFF(2,"Aberystwyth to Cardiff",
    Locations.ABERYSTWYTH, Locations.CARDIFF, null ),

    BELFAST_DUBLIN(4,"Belfast to Doublin",
    Locations.BELFAST ,Locations.DUBLIN, null  ), 

    BELFAST_MANCHESTER(9,"Belfast to Manchester",
    Locations.BELFAST ,Locations.MANCHESTER, null  ),

    BIRMINGHAM_CAMBRIDGE(2,"Birmingham to Cambridge",
    Locations.BIRMINGHAM ,Locations.CAMBRIDGE, null  ),

    BIRMINGHAM_LONDON(4,"Birmingham to London",
    Locations.BIRMINGHAM ,Locations.LONDON, null  ),

    BRISTOL_SOUTHAMPTON(2,"Bristol to Southhampton",
    Locations.BRISTOL ,Locations.SOUTHAMPTON, null  ),

    CAMBRIDGE_LONDON(3,"Cambridge to London",
    Locations.CAMBRIDGE ,Locations.LONDON , null ),

    CARDIFF_LONDON(8,"Cardiff to London",
    Locations.CARDIFF ,Locations.LONDON, null  ), 

    CARDIFF_READING(4,"Cardiff to Reading",
    Locations.CARDIFF ,Locations.READING, null  ),

    CORK_LEEDS(13,"Cork to Leeds",
    Locations.CORK ,Locations.LEEDS, null  ),

    DUBLIN_CORK(6,"Dublin to Cork",
    Locations.DUBLIN ,Locations.CORK, null  ),

    DUBLIN_LONDON(15,"Dublin to London",
    Locations.DUBLIN ,Locations.LONDON , null ),

    DUNDALK_CARLISLE(7,"Dundalk to Carlisle",
    Locations.DUNDALK ,Locations.CARLISLE , null ),

    EDINBURGH_BIRMINGHAM(12,"Edinburgh to Birmingham",
    Locations.EDINBURGH ,Locations.BIRMINGHAM, null  ),

    EDINBURGH_LONDON(15,"Edinburgh to London",
    Locations.EDINBURGH ,Locations.LONDON, null  ),

    FORTWILLIAM_EDINBURGH(3,"Fort William to Edinburgh",
    Locations.FORT_WILLIAM ,Locations.EDINBURGH, null  ),

    GALWAY_BARROW(3,"Galway to Barrow",
    Locations.GALWAY ,Locations.BARROW, null  ),

    GALWAY_DUBLIN(5,"Galway to Dublin",
    Locations.GALWAY ,Locations.DUBLIN, null  ),

    GLASGOW_DUBLIN(9,"Glasgow to Dublin",
    Locations.GLASGOW ,Locations.DUBLIN, null  ),

    GLASGOW_FRANCE(19,"Glasgow to France",
    Locations.GLASGOW ,Locations.FRANCE1, Locations.FRANCE2  ),

    GLASGOW_MANCHESTER(11,"Glasgow to Manchester",
    Locations.GLASGOW ,Locations.MANCHESTER, null  ), 
 
    HOLYHEAD_CARDIFF(4,"Holy Head to Cardiff",
    Locations.HOLY_HEAD ,Locations.CARDIFF, null  ),

    INVERNESS_BELFAST(10,"Inverness to Belfast",
    Locations.INVERNESS ,Locations.BELFAST, null  ),

    INVERNESS_LEEDS(13,"Inverness to Leeds",
    Locations.INVERNESS ,Locations.LEEDS, null  ),

    LEEDS_FRANCE(10,"Leeds to France",
    Locations.LEEDS ,Locations.FRANCE1, Locations.FRANCE2  ),

    LEEDS_LONDON(6,"Leeds to London",
    Locations.LEEDS ,Locations.LONDON, null  ),

    LEEDS_MANCHESTER(1,"Leeds to Manchester",
    Locations.LEEDS ,Locations.MANCHESTER, null  ),

    LIMERICK_CARDIFF(12,"Limerick to Cardiff",
    Locations.LIMERICK ,Locations.CARDIFF, null  ),

    LIVERPOOL_HULL(3,"Liverpool to Hull",
    Locations.LIVERPOOL ,Locations.HULL, null  ),

    LIVERPOOL_LLANDRINDOD(6,"Liverpool to Llandrindod",
    Locations.LIVERPOOL ,Locations.LLANDRINDOD_WELLS, null  ),

    LIVERPOOL_SOUTHAMPTON(6,"Liverpool to Southampton",
    Locations.LIVERPOOL ,Locations.SOUTHAMPTON, null  ),

    LONDON_BRIGHTON(3,"London to Brighton",
    Locations.LONDON ,Locations.BRIGHTON, null  ),

    LONDON_FRANCE(7,"London to France",
    Locations.LONDON ,Locations.FRANCE1, Locations.FRANCE2  ),

    LONDONDERRY_BIRMINGHANM(15,"Londonderry to Birmingham",
    Locations.LONDONDERRY ,Locations.BIRMINGHAM, null  ),

    LONDONDERRY_DUBLIN(6,"Londonderry to Dublin",
    Locations.LONDONDERRY ,Locations.DUBLIN, null  ),

    LONDONDERRY_STRANRAER(4,"Londonderry to Stranraer",
    Locations.LONDONDERRY ,Locations.STRANRAER, null  ),

    MANCHESTER_LONDON(6,"Manchester to London",
    Locations.MANCHESTER ,Locations.LONDON, null  ),

    MANCHESTER_NORWICH(6,"Manchester to Norwich",
    Locations.MANCHESTER ,Locations.NORWICH, null  ),

    MANCHESTER_PLYMOUTH(8,"Manchester to Plymouth",
    Locations.MANCHESTER ,Locations.PLYMOUTH, null  ),

    NEWCASTLE_HULL(3,"Newcastle to Hull",
    Locations.NEWCASTLE ,Locations.HULL, null  ),

    NEWCASTLE_SOUTHAMPTON(7,"Newcastle to Southampton",
    Locations.NEWCASTLE ,Locations.SOUTHAMPTON, null  ),

    NORTHAMPTON_DOVER(3,"Northampton to Dover",
    Locations.NORTHAMPTON ,Locations.DOVER, null  ),

    NORWICH_IPSWICH(1,"Norwich to Ipswich",
    Locations.NORWICH ,Locations.IPSWICH, null  ),

    NOTTINGHAM_IPSWICH(3,"Nottingham to Ipswich",
    Locations.NOTTINGHAM ,Locations.IPSWICH, null  ),

    PENZANCE_LONDON(10,"Penzance to London",
    Locations.PENZANCE ,Locations.LONDON, null  ),

    PLYMOUTH_READING(5,"Plymouth to Reading",
    Locations.PLYMOUTH ,Locations.READING , null ),

    ROSSLARE_ABERYSTWYTH(4,"Rosslare to Aberystwyth",
    Locations.ROSSLARE ,Locations.ABERYSTWYTH, null  ),

    ROSSLARE_CARMARTHEN(6,"Rosslare to Carmarthen",
    Locations.ROSSLARE ,Locations.CARMARTHEN, null  ),

    SLIGO_HOLYHEAD(9,"Sligo to Holy Head",
    Locations.SLIGO ,Locations.HOLY_HEAD , null ),

    SOUTHAMPTON_LONDON(4,"Southampton to London",
    Locations.SOUTHAMPTON ,Locations.LONDON, null  ),

    STORNOWAY_ABERDEEN(5,"Southampton to Aberdeen",
    Locations.STORNOWAY ,Locations.ABERDEEN, null  ),

    STORNOWAY_GLASGOW(7,"Southampton to Glasgow",
    Locations.STORNOWAY ,Locations.GLASGOW , null ),

    STRANRAER_TULLAMORE(6,"Stranraer to Tullamore",
    Locations.STRANRAER ,Locations.TULLAMORE , null ),

    ULLAPOOL_DUNDEE(4,"Ullapool to Dundee",
    Locations.ULLAPOOL ,Locations.DUNDEE, null  ),

    WICK_DUNDEE(4,"Wick to Dundee",
    Locations.WICK ,Locations.DUNDEE, null  ),

    WICK_EDINBURGH(5,"Wick to Edinburgh",
    Locations.WICK ,Locations.EDINBURGH , null );

    Image pic=null; //the pic of the card
    int points; //the amount of points each card is worth
    String name=null; //the name of the card
    Locations start = null; //the start location
    Locations end = null; //the ending location
    Locations end2 =null; //the second ending location
    /**
     * This is the constructor for the destination cards
     * @param p - and int representing the points for the card
     * @param name - the name of the card
     * @param s - the starting location of the card
     * @param e1 - the ending location of the card
     * @param e2 - the second ending location of the card
     */
    Destination(int p,String name, Locations s, Locations e1, Locations e2){
        points = p;
        this.name=name;
        start = s;
        end = e1;
        end2 = e2;

        try{
            pic= ImageIO.read(new File(
            ".\\Ticket to Ride Photos\\Destination Cards\\"+this+".jpg"));
        }
        catch(Exception e){
            System.err.println(this);
        }
    }

    /**
     * This method returns the image of the destination card enum
     * @return pic -  the image of the destination card enum
     */
    public Image getImage(){
        return pic;
    }

    /**
     * This method returns the name of the enum
     * @return name - the name of the enum
     */
    public String getName(){
        return name;
    }

    /**
     * This method returns the starting location of the enum
     * @return start - the start location
     */
    public Locations getStart() {
        return start;
    }

    /**
     * This method returns the first end location of the enum
     * @return end - the first end location
     */
    public Locations getEnd() {
        return end;
    }

    /**
     * This method returns the second end location of the enum
     * @return end2 - the second end location
     */
    public Locations getEnd2() {
        return end2;
    }
}