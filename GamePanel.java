import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.applet.*;
/**
 * Write a description of class EndGamePanel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GamePanel extends Applet
{
    boolean startGame=true;
    Image start;
    Image end;
    /**
     * this method is for instanciating the Applet
     */
    public void init(){
        try{
            start=ImageIO.read(new File(".\\Ticket to Ride Photos\\"+
            "Opening.jpg"));
            end=ImageIO.read(new File(".\\Ticket to Ride Photos\\"+
            "Ending.jpg"));
        }
        catch(Exception e){
            System.err.println(e);
        }
        repaint();
    }

    /**
     * This changes method the picture of the applet
     */
    public void endGame(){
        startGame=false;
        repaint();
    } 

    /**
     * This method paints the applet
     * @param g Graphics
     */
    public void paint(Graphics g)
    {
        if(startGame){
            g.drawImage(start,0,0,getWidth(),getHeight(),null);
        }
        else{
            g.drawImage(end,0,0,getWidth(),getHeight(),null);
            
        }
    }
}
