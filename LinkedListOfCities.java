import java.io.*;
import java.util.*;
import java.util.regex.*;
public class LinkedListOfCities
{
    Scanner input;
    ArrayList<String> coordinates = new ArrayList<>();
    Locations start;
    /**
     * This class contains the rest of the needed information
     * about a path.
     */
    public class Path
    {
        //this is the city/location where the route ends
        Locations end;
        Locations start;
        //this is the needed color of train to claim the route, 
        //A NULL VALUE REPRESENTS A GREY PATH
        Train color;
        //this variable accounts for the propellor technology, 
        //true means the play needs the technology card
        //propellors to claim the route
        boolean propellor;
        //this is the total number of trains needed to claim the 
        //route, including locomotives        
        int length;
        //this is the total number of locomotive cards needed to
        //claim the route 
        int locamotives;
        boolean isAvailable;
        ArrayList<Integer> x;
        ArrayList<Integer> y;
        /**
         *  This is a contructor for making paths between cities
         *  @param start Locations
         *  @param e Locations
         *  @param c Train color type
         *  @param p boolean propellors
         *  @param len int length of path
         *  @param l number of Locamotives
         *  @param int startLine line to start reading from the
         *      coordinates in the arrayList<String>
         *  @param int endLine line to end reading from the
         *      coordinates in the arrayList<String>
         */
        public Path(Locations start,Locations e,Train c,
        boolean p, int len,int l,int startLine, int endLine)
        {
            this.start=start;
            end = e;
            color = c;
            propellor = p;
            length = len;
            locamotives = l;
            isAvailable=true;
            x=new ArrayList<>();
            y=new ArrayList<>();

            for(int j=startLine-1;j<endLine;j++){
                Pattern pamp=Pattern.compile("(\\d){1,3}");
                Matcher matt=pamp.matcher(coordinates.get(j));
                for(int i=0;i<4;i++){
                    matt.find();
                    x.add(Integer.parseInt(matt.group())-350);
                    matt.find();
                    y.add(Integer.parseInt(matt.group()));
                }
            }
        }
    }

    /**
     * This method is used to add all paths for any 
     * particular player
     * @param p Player in particular
     */
    public void addAll(Player p){
        ArrayList<LinkedListOfCities.Path> shallowCopy=
            p.getControlledPaths();
        for(int i=0;i<47;i++){
            for(int j=0;j<10;j++){
                if(Driver.deck.locationV.get(i).routes[j]!=null){
                    shallowCopy.add(Driver.deck.locationV.get(i)
                        .routes[j]);
                }
            }
        }
    }

    /**
     * This method is used to determine if the path is a double 
     * route
     * @param start Locations 
     * @param end2 Locations
     * @return int[] 
     */
    public int[] isDoubleRoute( Locations start, Locations end2){
        int count = 0;
        int[] indecies = new int[2];
        for (int i = 0; i < start.routes.length; i++) {
            if (start.routes[i] == null && count != 2) {
                return null;
            }
            if (start.routes[i].end == end2) {
                count++;
                if (count == 1) {
                    indecies[0] = i;
                }
                else if (count == 2) {
                    indecies[1] = i;
                    return indecies;
                }
            }
        }
        return null;
    }

    /**
     * This method is used to check and add paths between Locations
     * @param p Player 
     * @param start Locations
     * @param end Locations
     * @return boolean is Path
     */ 
    public boolean addPath(Player p, Locations start,
    Locations end){
        ArrayList<LinkedListOfCities.Path> shallowCopy=
            p.getControlledPaths();
        int to=getRouteIndex(start,end);
        int to2=getRouteIndex(end,start);
        int[] indecies = isDoubleRoute(start,end );

        if(to==-1||to2==-1){
            return false;
        }
        if ( indecies != null && Driver.players.length >= 3) {
            boolean worked = addDoublePath( p,  start,  end, 
                    indecies, to, to2, shallowCopy);
            if (worked) {
                return true;
            }
            return false;
        }

        
        if(start.routes[to].isAvailable){

            Path temp=start.routes[to];

            

            if ( indecies != null ) {
                if ( start.routes[ indecies[0] ] != temp ){
                    temp = start.routes[ indecies[0] ];
                }
                temp = start.routes[ indecies[1] ];
            }              
            boolean needPropellor = temp.propellor;
            int tiles=temp.length;
            int locs=temp.locamotives;
            int neededTiles=tiles-locs;
            if(tiles>p.getPiecesLeft()){
                return false;
            }
            else if(playerHasDPower(p)&&tiles>p.getPiecesLeft()-1){
                return false;
            }
            
            if ( Locations.SOUTHAMPTON.routes[6] == temp ||
            temp == Locations.NEW_YORK.routes[0] ) {
                boolean canAdd = addNewYork(p, start,
                        end, tiles, locs, neededTiles);
                if (!canAdd) {
                    return false;
                }
                return true;
            }

            Path temp2 = start.routes[to] ;
            if ( indecies != null ) {
                if ( start.routes[ indecies[1] ] != temp ){
                    temp = start.routes[ indecies[1] ];
                }
                temp2 = start.routes[ indecies[0] ];
            }
            int tiles2=temp2.length;

            if(p.getOwnedTech().contains(Technology.DIESEL_POWER)){
                neededTiles--;
                if (neededTiles == 0) neededTiles++;
            }
            String concession=null;
            if(end.getConcession()!=null){
                concession = end.getConcession().getName();
            }
            Path tempBack=samePathBack(temp);
            Path tempBack2=tempBack;
            if ( indecies != null ) {
                tempBack2=samePathBack(temp2);
            }
            if(tempBack==null){
                return false;
            }
            if ( end.concession!=null &&
            !playerHasConcession(p, end ) ) {
                concession = concession.charAt(0) +
                (concession.substring(1).toLowerCase() );
                Driver.map.error(" You do not have the "+
                    concession + " technology card to travel to "+
                    "this area");
                return false;
            }
            if (needPropellor && !playerHasPropellors(p) && 
            temp != Locations.SOUTHAMPTON.routes[6]  ) {
                Driver.map.error(" You do not have the Propellor"+
                    " technology card");
                return false;
            }
            if (tiles == 3 && !playerHasMechanicalStroker(p) ) {
                Driver.map.error(" You do not have the Mechanical"
                    +" Stroker technology card");
                return false;
            }
            if (tiles > 3 && !playerHasSuperheatedSteamBoiler(p) &&
            temp != Locations.SOUTHAMPTON.routes[6] ) {
                Driver.map.error(" You do not have the Superheated"+
                    " Steam Boiler technology card");
                return false;
            }

            //check concession cards
            int index =0;
            if(temp.color==null ){
                if (p.trainSelectCount[8] >= locs) {
                    index = hasAmountOfTiles(p, neededTiles);

                    if (index == -1) {
                        printFail(p,index);
                        return false;
                    }
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }
                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    if (indecies != null) {
                        shallowCopy.add(temp2);
                        shallowCopy.add(tempBack2);
                        temp2.isAvailable=false;
                        tempBack2.isAvailable=false;
                    }
                    p.decrementPiecesLeft(tiles);
                    updatePoints(p, tiles);
                    return true;
                }
                else {
                    return false;
                }
            }

            if(temp.color!=null){ 
                index = hasAmountOfTiles(p, neededTiles,
                    temp.color);
                if (index == -1) {
                    printFail(p,index);
                    return false;
                }
                if ((index == 8 ||(p.trainSelectCount[8] >= locs &&
                        (p.trainSelectCount[index])>=neededTiles))
                || (p.trainSelectCount[8] + 
                    p.trainSelectCount[index] >= locs + neededTiles )){
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }

                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    if (indecies != null) {
                        shallowCopy.add(temp2);
                        shallowCopy.add(tempBack2);
                        temp2.isAvailable=false;
                        tempBack2.isAvailable=false;
                    }
                    p.decrementPiecesLeft(tiles);
                    updatePoints(p, tiles);
                    return true;
                }
                else {
                    return false;
                }
            }

            if (indecies != null && temp2.color == null) {
                if (p.trainSelectCount[8] >= locs) {
                    index = hasAmountOfTiles(p, neededTiles);

                    if (index == -1) {
                        printFail(p,index);
                        return false;
                    }
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }
                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    shallowCopy.add(temp2);
                    shallowCopy.add(tempBack2);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    temp2.isAvailable=false;
                    tempBack2.isAvailable=false;
                    p.decrementPiecesLeft(tiles2);
                    updatePoints(p, tiles2);
                    return true;
                }
                else {
                    return false;
                }
            }

            else if (indecies != null && temp2.color != null) {
                index = hasAmountOfTiles(p, neededTiles, 
                    temp.color);
                if(index != -1){
                    printFail(p,index);
                    return false;
                }
                if ((index == 8 ||(p.trainSelectCount[8] >= locs &&
                        (p.trainSelectCount[index])>=neededTiles))
                || (p.trainSelectCount[8] + 
                    p.trainSelectCount[index] >= locs + neededTiles )) {
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }

                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    shallowCopy.add(temp2);
                    shallowCopy.add(tempBack2);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    temp2.isAvailable=false;
                    tempBack2.isAvailable=false;
                    p.decrementPiecesLeft(tiles2);
                    updatePoints(p, tiles2);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                printFail(p,index);
                return false;
            }
        }
        return true;
    }

    public boolean addNewYork(Player p, Locations start, 
    Locations end,int tiles, int locs,int neededTiles ){
        Path temp = Locations.SOUTHAMPTON.routes[6];
        ArrayList<LinkedListOfCities.Path> shallowCopy=p.getControlledPaths();
        Path tempBack=samePathBack(temp);
        int index =0;
        if(tiles>p.getPiecesLeft()||(playerHasDPower(p)&&
            tiles>p.getPiecesLeft()-1)){
            return false;
        }
        if(temp.color==null ){
            if (p.trainSelectCount[8] >= locs) {
                index = hasAmountOfTiles(p, neededTiles);

                if (index == -1) {
                    return false;
                }
                if (index == 8) {
                    for(int i=0;i<locs+neededTiles;i++){
                        p.trainSelectCount[8]--;
                        Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                    }
                }
                else {
                    for(int i=0;i<locs;i++){
                        p.trainSelectCount[8]--;
                        Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                    }
                    for(int i=0;i<neededTiles;i++){
                        removeTrainCards(p,index);
                    }  
                }
                shallowCopy.add(temp);
                temp.isAvailable=false;
                tempBack.isAvailable=false;
                p.decrementPiecesLeft(tiles);
                updatePoints(p, tiles);
                return true;
            }
        }
        else {
            Driver.map.error(p.trainSelectCount[index] + " ind" + index);
            Driver.map.error("You do not have enough selected" + 
                "cards \n Please select the proper amount of cards");
            return false;
        }
        return false;
    }

    /**
     * This methos is responsible for printing out why any 
     * transaction failed based on the train car index values
     * @param p Player
     * @param index int
     */
    public void printFail(Player p,int index){
        if(index!=-1){
            Driver.map.error(p.trainSelectCount[index] +
                " is less than the needed amount");
        }
        Driver.map.error("You do not have enough selected cards"+
            " \n Please select the proper amount of cards");
    }

    /**
     * This method is used to add the double paths when they are
     * ineffect for the two player mode
     * @param p Player
     * @param start Locations
     * @param end Locations
     * @param indecies int[]
     * @param to int
     * @param to2 int
     * @param shallowCopy ArrayList<LinkedListOfCities.Path> 
     * @return boolean
     */
    public boolean addDoublePath(Player p, Locations start,
    Locations end, int[] indecies, int to, int to2,
    ArrayList<LinkedListOfCities.Path> shallowCopy) {
        if(start.routes[ indecies[0] ].isAvailable){
            Path temp=start.routes[ indecies[0] ];
            boolean needPropellor = temp.propellor;
            int tiles=temp.length;
            int locs=temp.locamotives;
            int neededTiles=tiles-locs;
            if(p.getOwnedTech().contains(Technology.DIESEL_POWER)){
                neededTiles--;
                if (neededTiles == 0) neededTiles++;
            }

            if ( Locations.SOUTHAMPTON.routes[6] == temp || 
            temp == Locations.NEW_YORK.routes[0] ) {
                boolean canAdd = addNewYork(p, start, 
                        end, tiles, locs, neededTiles);
                if (!canAdd) {
                    return false;
                }
                return true;
            }

            String concession=null;
            if(end.getConcession()!=null){
                concession = end.getConcession().getName();
            }
            Path tempBack=samePathBack(temp);
            if(tempBack==null){
                return false;
            }

            if ( end.concession!=null &&!playerHasConcession(p, end ) ) {
                concession = concession.charAt(0) + 
                (concession.substring(1).toLowerCase() );
                Driver.map.error(" You do not have the "+ concession 
                    + " technology card to travel to this area");
                return false;
            }
            if (needPropellor && !playerHasPropellors(p) ) {
                Driver.map.error(" You do not have the" + 
                    "Propellor technology card");
                return false;
            }
            if (tiles == 3 && !playerHasMechanicalStroker(p) ) {
                Driver.map.error(" You do not have the Mechanical" +
                    "Stroker technology card");
                return false;
            }
            if (tiles > 3 && !playerHasSuperheatedSteamBoiler(p) && 
            temp != Locations.SOUTHAMPTON.routes[6] ) {
                Driver.map.error(" You do not have the Superheated"
                    + "Steam Boiler technology card");
                return false;
            }

            //check concession cards
            int index =0;
            if(temp.color==null ){
                if (p.trainSelectCount[8] >= locs) {
                    index = hasAmountOfTiles(p, neededTiles);

                    if (index == -1) {
                        printFail(p,index);
                        return false;
                    }
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }
                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    p.decrementPiecesLeft(tiles);
                    updatePoints(p, tiles);

                }
                else {
                    return false;
                }
            }
            else if(temp.color!=null){ 
                index = hasAmountOfTiles(p, neededTiles, temp.color);
                if (index == -1) {
                    printFail(p,index);
                    return false;
                }
                if ((index == 8 ||(p.trainSelectCount[8] >= locs &&
                        (p.trainSelectCount[index])>=neededTiles))
                || (p.trainSelectCount[8] + 
                    p.trainSelectCount[index] >= locs + neededTiles )) {
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }

                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    p.decrementPiecesLeft(tiles);
                    updatePoints(p, tiles);

                }
                else {
                    return false;
                }
            }

            else {
                Driver.map.error(p.trainSelectCount[index] + 
                    " ind" + index);
                Driver.map.error("You do not have enough selected"
                    + "cards \n Please select the proper amount of cards");
                return false;
            }
        }
        else if(start.routes[ indecies[1] ].isAvailable){
            Path temp=start.routes[ indecies[1] ];
            boolean needPropellor = temp.propellor;
            int tiles=temp.length;
            int locs=temp.locamotives;
            int neededTiles=tiles-locs;
            if(p.getOwnedTech().contains(Technology.DIESEL_POWER)){
                neededTiles--;
            }
            String concession = end.getConcession() +"";
            Path tempBack=samePathBack(temp);
            if(tempBack==null){
                return false;
            }
            if ( end.concession!=null &&!playerHasConcession(p, end ) ) {
                concession = concession.charAt(0) + 
                (concession.substring(1).toLowerCase() );
                Driver.map.error(" You do not have the "+ concession 
                    + " technology card to travel to this area");
                return false;
            }
            if (needPropellor && !playerHasPropellors(p) ) {
                Driver.map.error(" You do not have the Propellor"
                    + "technology card");
                return false;
            }
            if (tiles == 3 && !playerHasMechanicalStroker(p) ) {
                Driver.map.error(" You do not have the Mechanical"
                    + "Stroker technology card");
                return false;
            }
            if (tiles > 3 && !playerHasSuperheatedSteamBoiler(p) && 
            temp != Locations.SOUTHAMPTON.routes[6] ) {
                Driver.map.error(" You do not have the Superheated"
                    + "Steam Boiler technology card");
                return false;
            }
            //check concession cards
            int index =0;
            if(temp.color==null ){
                if (p.trainSelectCount[8] >= locs) {
                    index = hasAmountOfTiles(p, neededTiles);

                    if (index == -1) {
                        printFail(p,index);
                        return false;
                    }
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(
                                Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }
                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    p.decrementPiecesLeft(tiles);
                    updatePoints(p, tiles);

                }
                else {
                    return false;
                }
            }
            else if(temp.color!=null){ 
                index = hasAmountOfTiles(p, neededTiles, temp.color);
                if (index == -1) {
                    printFail(p,index);
                    return false;
                }
                if ((index == 8 ||(p.trainSelectCount[8] >= locs &&
                        (p.trainSelectCount[index])>=neededTiles))
                || (p.trainSelectCount[8] + 
                    p.trainSelectCount[index] >= locs + neededTiles )) {
                    if (index == 8) {
                        for(int i=0;i<locs+neededTiles;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                    }
                    else {
                        for(int i=0;i<locs;i++){
                            p.trainSelectCount[8]--;
                            Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                        }
                        for(int i=0;i<neededTiles;i++){
                            removeTrainCards(p,index);
                        }  
                    }

                    shallowCopy.add(temp);
                    shallowCopy.add(tempBack);
                    temp.isAvailable=false;
                    tempBack.isAvailable=false;
                    p.decrementPiecesLeft(tiles);
                    updatePoints(p, tiles);

                }
                else {
                    return false;
                }
            }

            else {
                Driver.map.error(p.trainSelectCount[index] + " ind" + index);
                Driver.map.error("You do not have enough selected"
                    + "cards \n Please select the proper amount of cards");
                return false;
            }
        }
        return true;
    }

    /**
     * This method is to add the same path back for the two
     * Locations 
     * @param s Path
     */

    public Path samePathBack(Path s){
        Path temp=null;
        for(int i=0;i<10;i++){
            if(s.end.routes[i]==null){
                continue;
            }
            if(s.color==s.end.routes[i].color && s.length==s.end.routes[i].length 
            && s.locamotives==s.end.routes[i].locamotives &&
            s.end.routes[i].end == s.start){
                return s.end.routes[i];
            }
        }
        return temp;
    }

    /**
     * This method is used for returning a list of all paths 
     * between cities owned by Player
     * @param p Player
     * @param start Locations
     * @return Path[]
     */
    public Path[] ownedRoutesFrom(Player p, Locations start){
        Path[] temp=new Path[10];
        for(int i=0;i<10;i++){
            if(p.getControlledPaths().contains(start.routes[i])){
                temp[i]=start.routes[i];
            }
        }
        return temp;
    }

    /**
     * This method is used for cheching if the player has the 
     * Concession needed
     * @param p Player
     * @param end Locations
     * @return boolean
     */
    public boolean playerHasConcession(Player p, Locations end) {
        if ( p.getOwnedTech().contains(end.getConcession() ) ) {
            return true;
        }
        return false;
    }

    /**
     * This method is used to remove the used cards from the Player
     * and add them to the discard pile
     * @param p Player
     * @param index int
     */
    public void removeTrainCards(Player p, int index) { 
        switch(index){
            case 0:
            p.trainSelectCount[0]--;
            Driver.deck.discardPile.add(Train.RED);
            break;
            case 1:
            p.trainSelectCount[1]--;
            Driver.deck.discardPile.add(Train.BLUE);
            break;
            case 2:
            p.trainSelectCount[2]--;
            Driver.deck.discardPile.add(Train.GREEN);
            break;
            case 3:
            p.trainSelectCount[3]--;
            Driver.deck.discardPile.add(Train.ORANGE);
            break;
            case 4:
            p.trainSelectCount[4]--;
            Driver.deck.discardPile.add(Train.WHITE);
            break;
            case 5:
            p.trainSelectCount[5]--;
            Driver.deck.discardPile.add(Train.BLACK);
            break;
            case 6:
            p.trainSelectCount[6]--;
            Driver.deck.discardPile.add(Train.PINK);
            break;
            case 7:
            p.trainSelectCount[7]--;
            Driver.deck.discardPile.add(Train.YELLOW);
            break;
            default:
            System.err.println(
                "Error: LinkedListOfCities || AddPath <Switch> || removeTrainCards");
            break;
        }
    }

    /**
     * This method is used to update the players points
     * @param p Player
     * @param tiles int
     */
    public void updatePoints(Player p, int tiles) {
        switch(tiles){
            case 1:
            p.addPoints(1);
            break;
            case 2:
            p.addPoints(2);
            break;
            case 3:
            p.addPoints(4);
            break;
            case 4:
            p.addPoints(7);
            break;
            case 5:
            p.addPoints(10);
            break;
            case 6:
            p.addPoints(15);
            break;
            case 10:
            p.addPoints(40);
            break;
            default:
            System.err.println(
                "Error: LinkedListOfCities || AddPath <Switch> || updatePoints");
            break;
        }
    }

    /**
     * This method is used for the check if the player has enough
     * trains selected
     * @param p Player
     * @param tileAmt int
     */
    public int hasAmountOfTiles(Player p, int tileAmt ) {
        for(int i=0;i<p.trainSelectCount.length;i++){
            if ( p.trainSelectCount[i] >= tileAmt ) {
                return i;
            }
        }
        return -1;
    }

    /**
     * This method is used for the check if the player has enough
     * trains selected
     * @param p Player
     * @param tileAmt int
     * @param Train tempColor
     */
    public int hasAmountOfTiles(Player p, int tileAmt, 
    Train tempColor ) {
        if ( p.trainSelectCount[0] >= tileAmt &&
        tempColor == Train.RED) {
            return 0;
        }
        else if ( p.trainSelectCount[1] >= tileAmt &&
        tempColor == Train.BLUE) {
            return 1;
        }
        else if ( p.trainSelectCount[2] >= tileAmt &&
        tempColor == Train.GREEN) {
            return 2;
        }
        else if ( p.trainSelectCount[3] >= tileAmt &&
        tempColor == Train.ORANGE) {
            return 3;
        }
        else if ( p.trainSelectCount[4] >= tileAmt &&
        tempColor == Train.WHITE) {
            return 4;
        }
        else if ( p.trainSelectCount[5] >= tileAmt &&
        tempColor == Train.BLACK) {
            return 5;
        }
        else if ( p.trainSelectCount[6] >= tileAmt &&
        tempColor == Train.PINK) {
            return 6;
        }
        else if ( p.trainSelectCount[7] >= tileAmt &&
        tempColor == Train.YELLOW) {
            return 7;
        }
        else if ( p.trainSelectCount[8] >= tileAmt) {
            return 8;
        }
        else {
            return -1;
        }
    }

    /**
     * This method checks if the player has the 
     * SuperheatedSteamBoiler Technology
     * @param p Player
     * @return boolean
     */

    public boolean playerHasSuperheatedSteamBoiler(Player p) {
        if ( p.getOwnedTech().contains(
            Technology.SUPERHEATED_STEAM_BOILER) ) {
            return true;
        }
        return false;
    }

    /**
     * This method checks if the player has the 
     * Propellors Technology
     * @param p Player
     * @return boolean
     */
    public boolean playerHasPropellors(Player p) {
        if ( p.getOwnedTech().contains(Technology.PROPELLORS) ) {
            return true;
        }
        return false;
    }

    /**
     * This method checks if the player has the 
     * Diesel Power Technology
     * @param p Player
     * @return boolean
     */
    public boolean playerHasDPower(Player p) {
        if ( p.getOwnedTech().contains(Technology.DIESEL_POWER) ) {
            return true;
        }
        return false;
    }

    /**
     * This method checks if the player has the 
     * MechanicalStroker Technology
     * @param p Player
     * @return boolean
     */
    public boolean playerHasMechanicalStroker(Player p) {
        if ( p.getOwnedTech().contains(
            Technology.MECHANICAL_STROKER) ) {
            return true;
        }
        return false;
    }

    /**
     * This method is used to get the index of the path to a 
     * destinated city in reference to that city
     * @param a Locations
     * @param needed Locations
     */
    public int getRouteIndex(Locations a, Locations needed){
        for(int i=0;i<a.routes.length;i++){
            if(a.routes[i]==null){break;}
            if(a.routes[i].end==needed){
                return i;
            }
        }
        return -1;
    }

    /**
     * This is the constructer that adds all the paths between 
     * cities
     */
    public LinkedListOfCities()
    {
        try(Scanner in=new Scanner(new File("Coordinates.txt"))){
            while(in.hasNextLine()){
                coordinates.add(in.nextLine());
            }
        }catch(IOException e){
            System.err.println("File Not Found, LinkedListOfCities");
        }
        start = (Locations.STORNOWAY);
        Locations.STORNOWAY.routes[0] = new Path(
            start, Locations.WICK,null,true,5,2,56,60);
        Locations.STORNOWAY.routes[1] = new Path(
            start, Locations.ULLAPOOL,null,true,1,1,64,64);
        Locations.STORNOWAY.routes[2] = new Path(
            start, Locations.FORT_WILLIAM,null,true,5,1, 51,55);

        start = (Locations.ULLAPOOL);
        Locations.ULLAPOOL.routes[0] = new Path(
            start, Locations.STORNOWAY,null,true,1,1,64,64);
        Locations.ULLAPOOL.routes[1] = new Path(
            start, Locations.WICK,Train.YELLOW,false,3,0,61,63);
        Locations.ULLAPOOL.routes[2] = new Path(
            start, Locations.INVERNESS,Train.ORANGE,false,1,0,67,67);
        Locations.ULLAPOOL.routes[3] = new Path(
            start, Locations.FORT_WILLIAM,Train.PINK,false,2,0, 65,65);

        start = (Locations.WICK);
        Locations.WICK.routes[0] = new Path(
            start, Locations.STORNOWAY,null,true,5,2,56,60);
        Locations.WICK.routes[1] = new Path(
            start, Locations.ULLAPOOL,Train.YELLOW,false,3,0,61,63);
        Locations.WICK.routes[2] = new Path(
            start, Locations.INVERNESS,Train.RED,false,2,0,68,68);
        Locations.WICK.routes[3] = new Path(
            start, Locations.ABERDEEN,null,true,3,1,1,1);

        start = (Locations.INVERNESS);
        Locations.INVERNESS.routes[0] = new Path(
            start, Locations.ULLAPOOL,Train.ORANGE,false,1,0, 67, 67);
        Locations.INVERNESS.routes[1] = new Path(
            start, Locations.WICK,Train.RED,false,2,0, 68,68);
        Locations.INVERNESS.routes[2] = new Path(
            start, Locations.ABERDEEN,Train.PINK,false,3,0,69,71);
        Locations.INVERNESS.routes[3] = new Path(
            start, Locations.DUNDEE,Train.BLUE,false,3,0,73,73);
        Locations.INVERNESS.routes[4] = new Path(
            start, Locations.FORT_WILLIAM,Train.BLACK,false,2,0,66,66);

        start = (Locations.FORT_WILLIAM);
        Locations.FORT_WILLIAM.routes[0] = new Path(
            start, Locations.STORNOWAY,null,true,5,1,51,55);
        Locations.FORT_WILLIAM.routes[1] = new Path(
            start, Locations.ULLAPOOL,Train.PINK,false,2,0,65,65);
        Locations.FORT_WILLIAM.routes[2] = new Path(
            start, Locations.INVERNESS,Train.BLACK,false,2,0,66,66);
        Locations.FORT_WILLIAM.routes[3] = new Path(
            start, Locations.DUNDEE,Train.GREEN,false,3,0,74,74);
        Locations.FORT_WILLIAM.routes[4] = new Path(
            start, Locations.GLASGOW,Train.ORANGE,false,2,0,75,75);
        Locations.FORT_WILLIAM.routes[5] = new Path(
            start, Locations.LONDONDERRY,null,true,5,1,50,50);

        start = (Locations.ABERDEEN);
        Locations.ABERDEEN.routes[0] = new Path(
            start, Locations.WICK,null,true,3,1,1,1);
        Locations.ABERDEEN.routes[1] = new Path(
            start, Locations.NEWCASTLE,null,true,6,1,2,5);
        Locations.ABERDEEN.routes[2] = new Path(
            start, Locations.EDINBURGH,null,true,4,1,84,87);
        Locations.ABERDEEN.routes[3] = new Path(
            start, Locations.DUNDEE,Train.WHITE,false,1,0,72,72);
        Locations.ABERDEEN.routes[4] = new Path(
            start, Locations.INVERNESS,Train.PINK,false,3,0,69,71);

        start = (Locations.DUNDEE);
        Locations.DUNDEE.routes[0] = new Path(
            start, Locations.INVERNESS,Train.BLUE,false,3,0,73,73);
        Locations.DUNDEE.routes[1] = new Path(
            start, Locations.ABERDEEN,Train.WHITE,false,1,0,72,72);
        Locations.DUNDEE.routes[2] = new Path(
            start, Locations.EDINBURGH,Train.RED,false,1,0,83,83);
        Locations.DUNDEE.routes[3] = new Path(
            start, Locations.EDINBURGH,Train.YELLOW,false,1,0,82,82);
        Locations.DUNDEE.routes[4] = new Path(
            start, Locations.FORT_WILLIAM,Train.GREEN,false,3,0,74,74);

        start = (Locations.GLASGOW);
        Locations.GLASGOW.routes[0] = new Path(
            start, Locations.FORT_WILLIAM,Train.ORANGE,false,2,0,75,75);
        Locations.GLASGOW.routes[1] = new Path(
            start, Locations.EDINBURGH,Train.BLUE,false,1,0,81,81);
        Locations.GLASGOW.routes[2] = new Path(
            start, Locations.EDINBURGH,Train.BLACK,false,1,0,80,80);
        Locations.GLASGOW.routes[3] = new Path(
            start, Locations.STRANRAER,Train.RED,false,2,0,78,78);
        Locations.GLASGOW.routes[4] = new Path(
            start, Locations.LONDONDERRY,null,false,4,1,76,76);

        start = (Locations.EDINBURGH);
        Locations.EDINBURGH.routes[0] = new Path(
            start, Locations.DUNDEE,Train.YELLOW,false,1,0,82,82);
        Locations.EDINBURGH.routes[1] = new Path(
            start, Locations.DUNDEE,Train.RED,false,1,0,83,83);
        Locations.EDINBURGH.routes[2] = new Path(
            start, Locations.ABERDEEN,null,true,4,1,84,87);
        Locations.EDINBURGH.routes[3] = new Path(
            start, Locations.NEWCASTLE,Train.PINK,false,3,0,88,88);
        Locations.EDINBURGH.routes[4] = new Path(
            start, Locations.NEWCASTLE,Train.GREEN,false,3,0,89,89);
        Locations.EDINBURGH.routes[5] = new Path(
            start, Locations.CARLISLE,Train.ORANGE,false,2,0,90,90);
        Locations.EDINBURGH.routes[6] = new Path(
            start, Locations.STRANRAER,Train.WHITE,false,3,0,79,79);
        Locations.EDINBURGH.routes[7] = new Path(
            start, Locations.GLASGOW,Train.BLACK,false,1,0,80,80);
        Locations.EDINBURGH.routes[8] = new Path(
            start, Locations.GLASGOW,Train.BLUE,false,1,0,81,81);

        start = (Locations.LONDONDERRY);
        Locations.LONDONDERRY.routes[0] = new Path(
            start, Locations.FORT_WILLIAM,null,true,5,1,50,50);
        Locations.LONDONDERRY.routes[1] = new Path(
            start, Locations.GLASGOW,null,false,4,1,76,76);
        Locations.LONDONDERRY.routes[2] = new Path(
            start, Locations.STRANRAER,null,true,3,1,77,77);
        Locations.LONDONDERRY.routes[3] = new Path(
            start, Locations.BELFAST,Train.ORANGE,false,2,0,101,101);
        Locations.LONDONDERRY.routes[4] = new Path(
            start, Locations.DUNDALK,Train.PINK,false,3,0,102,104);
        Locations.LONDONDERRY.routes[5] = new Path(
            start, Locations.SLIGO,Train.GREEN,false,2,0,49,49);

        start = (Locations.SLIGO);
        Locations.SLIGO.routes[0] = new Path(
            start, Locations.LONDONDERRY,Train.GREEN,false,2,0,49,49);
        Locations.SLIGO.routes[1] = new Path(
            start, Locations.DUNDALK,Train.BLACK,false,3,0,105,105);
        Locations.SLIGO.routes[2] = new Path(
            start, Locations.TULLAMORE,Train.BLUE,false,3,0,106,106);
        Locations.SLIGO.routes[3] = new Path(
            start, Locations.GALWAY,Train.ORANGE,false,2,0,48,48);

        start = (Locations.BELFAST);
        Locations.BELFAST.routes[0] = new Path(
            start, Locations.LONDONDERRY,Train.ORANGE,false,2,0,101,101);
        Locations.BELFAST.routes[1] = new Path(
            start, Locations.STRANRAER,null,true,1,1,100,100);
        Locations.BELFAST.routes[2] = new Path(
            start, Locations.BARROW,null,true,4,1,149,149);
        Locations.BELFAST.routes[3] = new Path(
            start, Locations.DUNDALK,Train.RED,false,1,0,148,148);
        Locations.BELFAST.routes[4] = new Path(
            start, Locations.DUNDALK,Train.WHITE,false,1,0,147,147);

        start = (Locations.STRANRAER);
        Locations.STRANRAER.routes[0] = new Path(
            start, Locations.GLASGOW,Train.RED,false,2,0,78,78);
        Locations.STRANRAER.routes[1] = new Path(
            start, Locations.EDINBURGH,Train.WHITE,false,3,0,79,79);
        Locations.STRANRAER.routes[2] = new Path(
            start, Locations.CARLISLE,null,true,3,1,97,99);
        Locations.STRANRAER.routes[3] = new Path(
            start, Locations.BELFAST,null,true,1,1,100,100);
        Locations.STRANRAER.routes[4] = new Path(
            start, Locations.LONDONDERRY,null,true,3,1,77,77);

        start = (Locations.GALWAY);
        Locations.GALWAY.routes[0] = new Path(
            start, Locations.SLIGO,Train.ORANGE,false,2,0,48,48);
        Locations.GALWAY.routes[1] = new Path(
            start, Locations.TULLAMORE,null,false,2,0,107,107);
        Locations.GALWAY.routes[2] = new Path(
            start, Locations.TULLAMORE,null,false,2,0,108,108);
        Locations.GALWAY.routes[3] = new Path(
            start, Locations.LIMERICK,Train.YELLOW,false,1,0,47,47);

        start = (Locations.DUNDALK);
        Locations.DUNDALK.routes[0] = new Path(
            start, Locations.LONDONDERRY,Train.PINK,false,3,0,102,104);
        Locations.DUNDALK.routes[1] = new Path(
            start, Locations.BELFAST,Train.WHITE,false,1,0,147,147);
        Locations.DUNDALK.routes[2] = new Path(
            start, Locations.BELFAST,Train.RED,false,1,0,148,148);
        Locations.DUNDALK.routes[3] = new Path(
            start, Locations.HOLY_HEAD,null,true,3,1,146,146);
        Locations.DUNDALK.routes[4] = new Path(
            start, Locations.DUBLIN,Train.BLUE,false,1,0,144,144);
        Locations.DUNDALK.routes[5] = new Path(
            start, Locations.DUBLIN,Train.YELLOW,false,1,0,143,143);
        Locations.DUNDALK.routes[6] = new Path(
            start, Locations.SLIGO,Train.BLACK,false,3,0,105,105);

        start = (Locations.CARLISLE);
        Locations.CARLISLE.routes[0] = new Path(
            start, Locations.EDINBURGH,Train.ORANGE,false,2,0,90,90);
        Locations.CARLISLE.routes[1] = new Path(
            start, Locations.NEWCASTLE,Train.YELLOW,false,1,0,91,91);
        Locations.CARLISLE.routes[2] = new Path(
            start, Locations.BARROW,Train.RED,false,1,0,96,96);
        Locations.CARLISLE.routes[3] = new Path(
            start, Locations.STRANRAER,null,true,3,1,97,99);

        start = (Locations.NEWCASTLE);
        Locations.NEWCASTLE.routes[0] = new Path(
            start, Locations.EDINBURGH,Train.GREEN,false,3,0,89,89);
        Locations.NEWCASTLE.routes[1] = new Path(
            start, Locations.EDINBURGH,Train.PINK,false,3,0,88,88);
        Locations.NEWCASTLE.routes[2] = new Path(
            start, Locations.ABERDEEN,null,true,6,1,2,5);
        Locations.NEWCASTLE.routes[3] = new Path(
            start, Locations.HULL,null,true,5,1,6,10);
        Locations.NEWCASTLE.routes[4] = new Path(
            start, Locations.LEEDS,Train.ORANGE,false,2,0,92,93);
        Locations.NEWCASTLE.routes[5] = new Path(
            start, Locations.LEEDS,Train.WHITE,false,2,0,93,93);
        Locations.NEWCASTLE.routes[6] = new Path(
            start, Locations.CARLISLE,Train.YELLOW,false,1,0,91,91);    

        start = (Locations.LIMERICK);
        Locations.LIMERICK.routes[0] = new Path(
            start, Locations.GALWAY,Train.YELLOW,false,1,0,47,47);
        Locations.LIMERICK.routes[1] = new Path(
            start, Locations.TULLAMORE,null,false,1,0,109,109);
        Locations.LIMERICK.routes[2] = new Path(
            start, Locations.CORK,Train.PINK,false,1,0,46,46);

        start = (Locations.TULLAMORE);
        Locations.TULLAMORE.routes[0] = new Path(
            start, Locations.SLIGO,Train.BLUE,false,3,0,106,106);
        Locations.TULLAMORE.routes[1] = new Path(
            start, Locations.DUBLIN,Train.GREEN,false,1,0,142,142);
        Locations.TULLAMORE.routes[2] = new Path(
            start, Locations.DUBLIN,Train.ORANGE,false,1,0,141,141);
        Locations.TULLAMORE.routes[3] = new Path(
            start, Locations.ROSSLARE,Train.RED,false,2,0,140,140);
        Locations.TULLAMORE.routes[4] = new Path(
            start, Locations.CORK,Train.YELLOW,false,3,0,110,110);
        Locations.TULLAMORE.routes[5] = new Path(
            start, Locations.LIMERICK,null,false,1,0,109,109);            
        Locations.TULLAMORE.routes[6] = new Path(
            start, Locations.GALWAY,null,false,2,0,107,107);
        Locations.TULLAMORE.routes[7] = new Path(
            start, Locations.GALWAY,null,false,2,0,108,108);

        start = (Locations.DUBLIN);
        Locations.DUBLIN.routes[0] = new Path(
            start, Locations.DUNDALK,Train.YELLOW,false,1,0,143,143);
        Locations.DUBLIN.routes[1] = new Path(
            start, Locations.DUNDALK,Train.BLUE,false,1,0,144,144);
        Locations.DUBLIN.routes[2] = new Path(
            start, Locations.HOLY_HEAD,null,true,2,1,145,145);
        Locations.DUBLIN.routes[3] = new Path(
            start, Locations.ROSSLARE,Train.BLACK,false,2,0,138,138);
        Locations.DUBLIN.routes[4] = new Path(
            start, Locations.ROSSLARE,Train.WHITE,false,2,0,139,139);
        Locations.DUBLIN.routes[5] = new Path(
            start, Locations.TULLAMORE,Train.ORANGE,false,1,0,141,141);
        Locations.DUBLIN.routes[6] = new Path(
            start, Locations.TULLAMORE,Train.GREEN,false,1,0,142,142);

        start = (Locations.BARROW);
        Locations.BARROW.routes[0] = new Path(
            start, Locations.CARLISLE,Train.RED,false,1,0,96,96);
        Locations.BARROW.routes[1] = new Path(
            start, Locations.LEEDS,Train.GREEN,false,2,0,94,95);
        Locations.BARROW.routes[2] = new Path(
            start, Locations.LIVERPOOL,null,true,1,1,152,152);
        Locations.BARROW.routes[3] = new Path(
            start, Locations.BELFAST,null,true,4,1,149,149);

        start = (Locations.CORK);
        Locations.CORK.routes[0] = new Path(
            start, Locations.LIMERICK,Train.PINK,false,1,0,46,46);
        Locations.CORK.routes[1] = new Path(
            start, Locations.TULLAMORE,Train.YELLOW,false,3,0,110,110);
        Locations.CORK.routes[2] = new Path(
            start, Locations.ROSSLARE,Train.BLUE,false,2,0,111,111);
        Locations.CORK.routes[3] = new Path(
            start, Locations.PENZANCE,null,true,6,2,45,45);

        start = (Locations.ROSSLARE);
        Locations.ROSSLARE.routes[0] = new Path(
            start, Locations.TULLAMORE,Train.RED,false,2,0,140,140);
        Locations.ROSSLARE.routes[1] = new Path(
            start, Locations.DUBLIN,Train.WHITE,false,2,0,139,139);
        Locations.ROSSLARE.routes[2] = new Path(
            start, Locations.DUBLIN,Train.BLACK,false,2,0,138,138);
        Locations.ROSSLARE.routes[3] = new Path(
            start, Locations.HOLY_HEAD,null,true,3,1,137,137);
        Locations.ROSSLARE.routes[4] = new Path(
            start, Locations.ABERYSTWYTH,null,true,3,1,136,136);
        Locations.ROSSLARE.routes[5] = new Path(
            start, Locations.CARMARTHEN,null,true,4,1,112,115);
        Locations.ROSSLARE.routes[6] =new Path(
            start, Locations.CORK,Train.BLUE,false,2,0,111,111);

        start = (Locations.HOLY_HEAD);
        Locations.HOLY_HEAD.routes[0] = new Path(
            start, Locations.LIVERPOOL,null,true,2,1,150,151);
        Locations.HOLY_HEAD.routes[1] = new Path(
            start, Locations.LLANDRINDOD_WELLS,Train.BLUE,false,3,0,185,185);
        Locations.HOLY_HEAD.routes[2] = new Path(
            start, Locations.ABERYSTWYTH,null,true,2,1,187,188);
        Locations.HOLY_HEAD.routes[3] = new Path(
            start, Locations.ROSSLARE,null,true,3,1,137,137);
        Locations.HOLY_HEAD.routes[4] = new Path(
            start, Locations.DUBLIN,null,true,2,1,145,145);
        Locations.HOLY_HEAD.routes[5] = new Path(
            start, Locations.DUNDALK,null,true,3,1,146,146);

        start = (Locations.LIVERPOOL);
        Locations.LIVERPOOL.routes[0] = new Path(
            start, Locations.BARROW,null,true,1,1,152,152);
        Locations.LIVERPOOL.routes[1] = new Path(
            start, Locations.LEEDS,Train.BLACK,false,2,0,153,153);
        Locations.LIVERPOOL.routes[2] = new Path(
            start, Locations.MANCHESTER,Train.ORANGE,false,1,0,184,184);
        Locations.LIVERPOOL.routes[3] = new Path(
            start, Locations.MANCHESTER,Train.PINK,false,1,0,183,183);
        Locations.LIVERPOOL.routes[4] = new Path(
            start, Locations.HOLY_HEAD,null,true,2,1,150,151);

        start = (Locations.MANCHESTER);
        Locations.MANCHESTER.routes[0] = new Path(
            start, Locations.LEEDS,Train.RED,false,1,0,154,154);
        Locations.MANCHESTER.routes[1] = new Path(
            start, Locations.LEEDS,Train.BLUE,false,1,0,155,155);
        Locations.MANCHESTER.routes[2] = new Path(
            start, Locations.BIRMINGHAM,Train.YELLOW,false,2,0,176,176);
        Locations.MANCHESTER.routes[3] = new Path(
            start, Locations.BIRMINGHAM,Train.BLACK,false,2,0,177,177);
        Locations.MANCHESTER.routes[4] = new Path(
            start, Locations.LLANDRINDOD_WELLS,Train.GREEN,false,3,0,180,182);
        Locations.MANCHESTER.routes[5] = new Path(
            start, Locations.LIVERPOOL,Train.PINK,false,1,0,183,183);
        Locations.MANCHESTER.routes[6] = new Path(
            start, Locations.LIVERPOOL,Train.ORANGE,false,1,0,184,184);

        start = (Locations.LEEDS);
        Locations.LEEDS.routes[0] = new Path(
            start, Locations.NEWCASTLE,Train.WHITE,false,2,0,93,93);
        Locations.LEEDS.routes[1] = new Path(
            start, Locations.NEWCASTLE,Train.ORANGE,false,2,0,92,92);
        Locations.LEEDS.routes[2] = new Path(
            start, Locations.HULL,Train.YELLOW,false,1,0,156,156);
        Locations.LEEDS.routes[3] = new Path(
            start, Locations.NOTTINGHAM,Train.PINK,false,2,0,157,157);
        Locations.LEEDS.routes[4] = new Path(
            start, Locations.MANCHESTER,Train.BLUE,false,1,0,155,155);
        Locations.LEEDS.routes[5] = new Path(
            start, Locations.MANCHESTER,Train.RED,false,1,0,154,154);
        Locations.LEEDS.routes[6] = new Path(
            start, Locations.LIVERPOOL,Train.BLACK,false,2,0,153,153);
        Locations.LEEDS.routes[7] = new Path(
            start, Locations.BARROW,Train.GREEN,false,2,0,94,95);            

        start = (Locations.HULL);
        Locations.HULL.routes[0] = new Path(
            start, Locations.NEWCASTLE,null,true,5,1,6,10);
        Locations.HULL.routes[1] = new Path(
            start, Locations.NORWICH,null,false,4,0,11,14);
        Locations.HULL.routes[2] = new Path(
            start, Locations.NOTTINGHAM,Train.BLACK,false,2,0,158,158);
        Locations.HULL.routes[3] = new Path(
            start, Locations.LEEDS,Train.YELLOW,false,1,0,156,156);

        start = (Locations.ABERYSTWYTH);
        Locations.ABERYSTWYTH.routes[0] = new Path(
            start, Locations.HOLY_HEAD,null,true,2,1,187,188);
        Locations.ABERYSTWYTH.routes[1] = new Path(
            start, Locations.LLANDRINDOD_WELLS,Train.WHITE,false,1,0,186,186);
        Locations.ABERYSTWYTH.routes[2] = new Path(
            start, Locations.CARMARTHEN,Train.YELLOW,false,1,0,135,135);
        Locations.ABERYSTWYTH.routes[3] = new Path(
            start, Locations.ROSSLARE,null,true,3,1,136,136);

        start = (Locations.LLANDRINDOD_WELLS);
        Locations.LLANDRINDOD_WELLS.routes[0] = new Path(
            start, Locations.HOLY_HEAD,Train.BLUE,false,3,0,185,185);
        Locations.LLANDRINDOD_WELLS.routes[1] = new Path(
            start, Locations.MANCHESTER,Train.GREEN,false,3,0,180,182);
        Locations.LLANDRINDOD_WELLS.routes[2] = new Path(
            start, Locations.BIRMINGHAM,Train.RED,false,2,0,178,179);
        Locations.LLANDRINDOD_WELLS.routes[3] = new Path(
            start, Locations.CARDIFF,Train.PINK,false,1,0,134,134);
        Locations.LLANDRINDOD_WELLS.routes[4] = new Path(
            start, Locations.ABERYSTWYTH,Train.WHITE,false,1,0,186,186);

        start = (Locations.BIRMINGHAM);
        Locations.BIRMINGHAM.routes[0] = new Path(
            start, Locations.MANCHESTER,Train.BLACK,false,2,0,177,177);
        Locations.BIRMINGHAM.routes[1] = new Path(
            start, Locations.MANCHESTER,Train.YELLOW,false,2,0,176,176);
        Locations.BIRMINGHAM.routes[2] = new Path(
            start, Locations.NOTTINGHAM,null,false,1,0,175,175);
        Locations.BIRMINGHAM.routes[3] = new Path(
            start, Locations.NORTHAMPTON,Train.GREEN,false,1,0,173,173);
        Locations.BIRMINGHAM.routes[4] = new Path(
            start, Locations.NORTHAMPTON,Train.WHITE,false,1,0,172,172);
        Locations.BIRMINGHAM.routes[5] = new Path(
            start, Locations.READING,null,false,2,0,170,171);
        Locations.BIRMINGHAM.routes[6] = new Path(
            start, Locations.CARDIFF,Train.ORANGE,false,3,0,132,132);
        Locations.BIRMINGHAM.routes[7] = new Path(
            start, Locations.CARDIFF,Train.BLUE,false,3,0,133,133);
        Locations.BIRMINGHAM.routes[8] = new Path(
            start, Locations.LLANDRINDOD_WELLS,Train.RED,false,2,0,178,179);

        start = (Locations.CARMARTHEN);
        Locations.CARMARTHEN.routes[0] = new Path(
            start, Locations.ABERYSTWYTH,Train.YELLOW,false,1,0,135,135);
        Locations.CARMARTHEN.routes[1] = new Path(
            start, Locations.CARDIFF,Train.RED,false,1,0,116,116);
        Locations.CARMARTHEN.routes[2] = new Path(
            start, Locations.ROSSLARE,null,true,4,1,112,115);

        start = (Locations.CARDIFF);
        Locations.CARDIFF.routes[0] = new Path(
            start, Locations.LLANDRINDOD_WELLS,Train.PINK,false,1,0,134,134);
        Locations.CARDIFF.routes[1] = new Path(
            start, Locations.BIRMINGHAM,Train.BLUE,false,3,0,133,133);
        Locations.CARDIFF.routes[2] = new Path(
            start, Locations.BIRMINGHAM,Train.ORANGE,false,3,0,132,132);
        Locations.CARDIFF.routes[3] = new Path(
            start, Locations.BRISTOL,null,false,1,1,131,131);
        Locations.CARDIFF.routes[4] = new Path(
            start, Locations.PENZANCE,null,true,5,1,117,121);
        Locations.CARDIFF.routes[5] = new Path(
            start, Locations.CARMARTHEN,Train.RED,false,1,0,116,116);

        start = (Locations.BRISTOL);
        Locations.BRISTOL.routes[0] = new Path(
            start, Locations.READING,Train.WHITE,false,2,0,130,130);
        Locations.BRISTOL.routes[1] = new Path(
            start, Locations.SOUTHAMPTON,Train.GREEN,false,2,0,124,124);
        Locations.BRISTOL.routes[2] = new Path(
            start, Locations.PLYMOUTH,Train.YELLOW,false,3,0,123,123);
        Locations.BRISTOL.routes[3] = new Path(
            start, Locations.CARDIFF,null,false,1,1,131,131);

        start = (Locations.NORTHAMPTON);
        Locations.NORTHAMPTON.routes[0] = new Path(
            start, Locations.NOTTINGHAM,Train.ORANGE,false,1,0,174,174);
        Locations.NORTHAMPTON.routes[1] = new Path(
            start, Locations.CAMBRIDGE,null,false,1,0,166,166);
        Locations.NORTHAMPTON.routes[2] = new Path(
            start, Locations.LONDON,Train.BLUE,false,1,0,167,167);
        Locations.NORTHAMPTON.routes[3] = new Path(
            start, Locations.LONDON,Train.PINK,false,1,0,168,168);
        Locations.NORTHAMPTON.routes[4] = new Path(
            start, Locations.READING,Train.RED,false,1,0,169,169);
        Locations.NORTHAMPTON.routes[5] = new Path(
            start, Locations.BIRMINGHAM,Train.WHITE,false,1,0,172,172);
        Locations.NORTHAMPTON.routes[6] = new Path(
            start, Locations.BIRMINGHAM,Train.GREEN,false,1,0,173,173);

        start = (Locations.CAMBRIDGE);
        Locations.CAMBRIDGE.routes[0] = new Path(
            start, Locations.NORWICH,Train.RED,false,2,0,161,162);
        Locations.CAMBRIDGE.routes[1] = new Path(
            start, Locations.IPSWICH,Train.BLACK,false,1,0,163,163);
        Locations.CAMBRIDGE.routes[2] = new Path(
            start, Locations.LONDON,Train.YELLOW,false,1,0,164,164);
        Locations.CAMBRIDGE.routes[3] = new Path(
            start, Locations.LONDON,Train.ORANGE,false,1,0,165,165);
        Locations.CAMBRIDGE.routes[4] = new Path(
            start, Locations.NORTHAMPTON,null,false,1,0,166,166);
        Locations.CAMBRIDGE.routes[5] = new Path(
            start, Locations.NOTTINGHAM,null,false,2,0,160,160);

        start = (Locations.NORWICH);
        Locations.NORWICH.routes[0] = new Path(
            start, Locations.IPSWICH,Train.GREEN,false,1,0,15,15);
        Locations.NORWICH.routes[1] = new Path(
            start, Locations.CAMBRIDGE,Train.RED,false,2,0,161,162);
        Locations.NORWICH.routes[2] = new Path(
            start, Locations.NOTTINGHAM,Train.WHITE,false,4,0,159,159);
        Locations.NORWICH.routes[3] = new Path(
            start, Locations.HULL,null,false,4,0,11,14);

        start = (Locations.READING);
        Locations.READING.routes[0] = new Path(
            start, Locations.NORTHAMPTON,Train.RED,false,1,0,169,169);
        Locations.READING.routes[1] = new Path(
            start, Locations.LONDON,Train.GREEN,false,1,0,129,129);
        Locations.READING.routes[2] = new Path(
            start, Locations.SOUTHAMPTON,Train.ORANGE,false,1,0,125,125);
        Locations.READING.routes[3] = new Path(
            start, Locations.BRISTOL,Train.WHITE,false,2,0,130,130);
        Locations.READING.routes[4] = new Path(
            start, Locations.BIRMINGHAM,null,false,2,0,170,171);

        start = (Locations.PENZANCE);
        Locations.PENZANCE.routes[0] = new Path(
            start, Locations.CORK,null,true,6,2,45,45);
        Locations.PENZANCE.routes[1] = new Path(
            start, Locations.CARDIFF,null,true,5,1,117,121);
        Locations.PENZANCE.routes[2] = new Path(
            start, Locations.PLYMOUTH,Train.BLACK,false,2,0,122,122);
        Locations.PENZANCE.routes[3] = new Path(
            start, Locations.PLYMOUTH,null,true,3,1,42,44);

        start = (Locations.PLYMOUTH);
        Locations.PLYMOUTH.routes[0] = new Path(
            start, Locations.BRISTOL,Train.YELLOW,false,3,0,123,123);
        Locations.PLYMOUTH.routes[1] = new Path(
            start, Locations.SOUTHAMPTON,null,true,5,1,37,41);
        Locations.PLYMOUTH.routes[2] = new Path(
            start, Locations.PENZANCE,null,true,3,1,42,44);
        Locations.PLYMOUTH.routes[3] = new Path(
            start, Locations.PENZANCE,Train.BLACK,false,2,0,122,122);

        start = (Locations.SOUTHAMPTON);
        Locations.SOUTHAMPTON.routes[0] = new Path(
            start, Locations.READING,Train.ORANGE,false,1,0,125,125);
        Locations.SOUTHAMPTON.routes[1] = new Path(
            start, Locations.LONDON,Train.RED,false,2,0,126,126);
        Locations.SOUTHAMPTON.routes[2] = new Path(
            start, Locations.LONDON,Train.BLACK,false,2,0,127,127);
        Locations.SOUTHAMPTON.routes[3] = new Path(
            start, Locations.BRIGHTON,Train.BLUE,false,1,0,20,20);
        Locations.SOUTHAMPTON.routes[4] = new Path(
            start, Locations.FRANCE1,null,true,3,1,21,23);
        Locations.SOUTHAMPTON.routes[5] = new Path(
            start, Locations.FRANCE1,null,true,3,1,24,26);
        Locations.SOUTHAMPTON.routes[6] = new Path(
            start, Locations.NEW_YORK,null,true,10,3,27,36); //add new york as a locations
        Locations.SOUTHAMPTON.routes[7] = new Path(
            start, Locations.PLYMOUTH,null,true,5,1,37,41);
        Locations.SOUTHAMPTON.routes[8] = new Path(
            start, Locations.BRISTOL,Train.GREEN,false,2,0,124,124);

        start = (Locations.LONDON);
        Locations.LONDON.routes[0] = new Path(
            start, Locations.CAMBRIDGE,Train.ORANGE,false,1,0,165,165);
        Locations.LONDON.routes[1] = new Path(
            start, Locations.CAMBRIDGE,Train.YELLOW,false,1,0,164,164);
        Locations.LONDON.routes[2] = new Path(
            start, Locations.IPSWICH,Train.WHITE,false,1,0,16,16);
        Locations.LONDON.routes[3] = new Path(
            start, Locations.DOVER,null,false,2,0,17,17);
        Locations.LONDON.routes[4] = new Path(
            start, Locations.BRIGHTON,null,false,1,0,128,128);
        Locations.LONDON.routes[5] = new Path(
            start, Locations.SOUTHAMPTON,Train.BLACK,false,2,0,127,127);
        Locations.LONDON.routes[6] = new Path(
            start, Locations.SOUTHAMPTON,Train.RED,false,2,0,126,126);
        Locations.LONDON.routes[7] = new Path(
            start, Locations.READING,Train.GREEN,false,1,0,129,129);
        Locations.LONDON.routes[8] = new Path(
            start, Locations.NORTHAMPTON,Train.PINK,false,1,0,168,168);
        Locations.LONDON.routes[9] = new Path(
            start, Locations.NORTHAMPTON,Train.BLUE,false,1,0,167,167);

        start = (Locations.BRIGHTON);
        Locations.BRIGHTON.routes[0] = new Path(
            start, Locations.LONDON,null,false,1,0,128,128);
        Locations.BRIGHTON.routes[1] = new Path(
            start, Locations.DOVER,Train.PINK,false,2,0,19,19);
        Locations.BRIGHTON.routes[2] = new Path(
            start, Locations.SOUTHAMPTON,Train.BLUE,false,1,0,20,20);

        start = (Locations.DOVER);
        Locations.DOVER.routes[0] = new Path(
            start, Locations.FRANCE2,null,true,2,1,18,18);
        Locations.DOVER.routes[1] = new Path(
            start, Locations.BRIGHTON,Train.PINK,false,2,0,19,19);
        Locations.DOVER.routes[2] = new Path(
            start, Locations.LONDON,null,false,2,0,17,17);

        start = (Locations.FRANCE1);
        Locations.FRANCE1.routes[0] = new Path(
            start, Locations.SOUTHAMPTON,null,true,3,1,21,23);
        Locations.FRANCE1.routes[1] = new Path(
            start, Locations.SOUTHAMPTON,null,true,3,1,24,26);            

        start = (Locations.FRANCE2);
        Locations.FRANCE2.routes[0] = new Path(
            start, Locations.DOVER,null,true,2,1,18,18);        

        start = (Locations.IPSWICH);
        Locations.IPSWICH.routes[0] = new Path(
            start, Locations.NORWICH,Train.GREEN,false,1,0,15,15);
        Locations.IPSWICH.routes[1] = new Path(
            start, Locations.LONDON,Train.WHITE,false,1,0,16,16);
        Locations.IPSWICH.routes[2] = new Path(
            start, Locations.CAMBRIDGE,Train.BLACK,false,1,0,163,163);

        start = (Locations.NOTTINGHAM);
        Locations.NOTTINGHAM.routes[0] = new Path(
            start, Locations.LEEDS,Train.PINK,false,2,0,157,157);
        Locations.NOTTINGHAM.routes[1] = new Path(
            start, Locations.HULL,Train.BLACK,false,2,0,158,158);
        Locations.NOTTINGHAM.routes[2] = new Path(
            start, Locations.NORWICH,Train.WHITE,false,4,0,159,159);
        Locations.NOTTINGHAM.routes[3] = new Path(
            start, Locations.CAMBRIDGE,null,false,2,0,160,160);
        Locations.NOTTINGHAM.routes[4] = new Path(
            start, Locations.NORTHAMPTON,Train.ORANGE,false,1,0,174,174);
        Locations.NOTTINGHAM.routes[5] = new Path(
            start, Locations.BIRMINGHAM,null,false,1,0,175,175);

        start = (Locations.NEW_YORK);
        Locations.NEW_YORK.routes[0] = new Path(
            start, Locations.SOUTHAMPTON,null,true,10,3,27,36);
    }
}