import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;
/**
 * Enumeration class Locations - Enum of all the Locations in Ticket to Ride UK edition
 * 
 * @author Team Snuffles
 * @version 1.0
 */
public enum Locations
{
    STORNOWAY (749,37, Technology.SCOTLAND_CONCESSION), 

    ULLAPOOL(787,86, Technology.SCOTLAND_CONCESSION), 

    WICK(914,85, Technology.SCOTLAND_CONCESSION), 

    INVERNESS(820,137, Technology.SCOTLAND_CONCESSION), 

    FORT_WILLIAM(736,186, Technology.SCOTLAND_CONCESSION), 

    ABERDEEN(904,225, Technology.SCOTLAND_CONCESSION), 

    DUNDEE(844,260, Technology.SCOTLAND_CONCESSION), 

    GLASGOW(742,291, Technology.SCOTLAND_CONCESSION),

    EDINBURGH(802,309, Technology.SCOTLAND_CONCESSION),

    LONDONDERRY(561,276, Technology.IRELAND_FRANCE_CONCESSION),

    SLIGO(456,308, Technology.IRELAND_FRANCE_CONCESSION), 

    BELFAST(605,361, Technology.IRELAND_FRANCE_CONCESSION), 

    STRANRAER(666,353, Technology.SCOTLAND_CONCESSION), 

    GALWAY(382,385, Technology.IRELAND_FRANCE_CONCESSION), 

    DUNDALK(553,399, Technology.IRELAND_FRANCE_CONCESSION), 

    CARLISLE(767,413, null), 

    NEWCASTLE(840,431, null), 

    LIMERICK(382,458,Technology.IRELAND_FRANCE_CONCESSION), 

    TULLAMORE(463,441, Technology.IRELAND_FRANCE_CONCESSION), 

    DUBLIN(526,459, Technology.IRELAND_FRANCE_CONCESSION), 

    BARROW(722,462, null), 

    CORK(368,532, Technology.IRELAND_FRANCE_CONCESSION), 

    ROSSLARE(473,546, Technology.IRELAND_FRANCE_CONCESSION), 

    HOLY_HEAD(617,507, Technology.WALES_CONCESSION), 

    LIVERPOOL(694,518, null),

    MANCHESTER(746,556, null), 

    LEEDS(799,525, null), 

    HULL(858,567, null), 

    ABERYSTWYTH(596,599, Technology.WALES_CONCESSION), 

    LLANDRINDOD_WELLS(638,644, Technology.WALES_CONCESSION), 

    BIRMINGHAM(725,655, null), 

    CARMARTHEN(560,655, Technology.WALES_CONCESSION), 

    CARDIFF(603,697, Technology.WALES_CONCESSION), 

    BRISTOL(644,736,null), 

    NORTHAMPTON(767,704, null), 

    CAMBRIDGE(827,722, null), 

    NORWICH(924,718, null), 

    READING(732,760, null), 

    PENZANCE(417,757, null), 

    PLYMOUTH(515,767, null), 

    SOUTHAMPTON(697,820, null), 

    LONDON(795,781, null), 

    BRIGHTON(764,848, null), 

    DOVER(866,852,null), 

    FRANCE1(646,930, Technology.IRELAND_FRANCE_CONCESSION), 

    FRANCE2(943,895, Technology.IRELAND_FRANCE_CONCESSION), 

    IPSWICH(877,774, null),

    NEW_YORK(355, 873, null),

    NOTTINGHAM(781,627, null);

    //Coordinates of the map itself, used for graphical 
    //purposes of selecting the city
    //TOP LEFT 349 25
    //TOP RIGHT 947 25
    //BOTTOM LEFT 349 938
    //BOTTOM RIGHT 947 938
    LinkedListOfCities.Path[] routes; //array of possible routes a city has 
    int x; //x value of city  
    int y; //y value of city
    Technology concession; //concession needed to enter area
    final int radius=50; //used for graphic hover effect

    /**
     *Constructor of Location enum
     *@param x the x value on the map passed in
     *@param y the y value on the map passed in
     *@param p the consession of the city, null if does not need one
     *
     */
    Locations(int x, int y, Technology p) {
        routes=new LinkedListOfCities.Path[10];
        this.x = x-350;
        this.y = y;
        concession = p;
    }

    /**
     * Getter method to get the concession in the enum class
     * @return concession the concession the enum contains
     */
    public Technology getConcession() {
        return concession;
    }

    /**
     * Helper method to see if the mouse is within the range for selection
     * @param e the mouse event
     * @return true if the sqrt of x^2 plus y^2 is less than or equal to 50
     */
    public boolean range(MouseEvent e){
        return (Math.hypot(x-e.getX(),y-e.getY()) <= radius);
    }

    /**
     *Debugging method to display text on the GUI for debugging purposes
     *@param s the string passed in that is to be displayed
     */
    public void debug(String s){
        JOptionPane.showMessageDialog(null,s,"Debugging String",
            JOptionPane.INFORMATION_MESSAGE);
    }
}
