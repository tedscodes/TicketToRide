import java.awt.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import javax.imageio.*;
/**
 * Enumeration class Train - Enum of trains used in ticket to ride, trains delineated by color
 * 
 * @author Team Snuffles
 * @version 1.0
 */
public enum Train {
    RED(),

    BLUE(),

    GREEN(),

    ORANGE(),

    WHITE(),

    BLACK(),

    PINK(),

    YELLOW(),

    LOCAMOTIVE();

}
