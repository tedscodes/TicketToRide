import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.io.*;
import javax.imageio.*;
/**
 * Enumeration class Technology - Enum class of the standard technologies in ticket to ride
 * 
 * @author Team Snuffles
 * @version 1.0
 */
public enum Technology
{
    BOOSTER(4,2,"Booster"), 

    BOILER_LAGGING(4,2,"Boiler Lagging"), 

    DIESEL_POWER(1,3,"Diesel Power"), 

    //DOUBLING_HEADING(4,4,"Doubling Heading"), 

    //EQUALISING_BEAM(1,2,"Equalising Beam"),

    IRELAND_FRANCE_CONCESSION(4,1,"Ireland & France Concession"),

    MECHANICAL_STROKER(4,1,"Mechanical Stroker"),

    PROPELLORS(4,2,"Propellors"), 

    //RIGHT_OF_WAY(1,4,"Right of Way"), 

    //RISKY_CONTRACTS(1,2,"Risky Contracts"), 

    SCOTLAND_CONCESSION(4,1,"Scotland Concession"), 

    STEAM_TURBINES(4,2,"Steam Turbines"), 

    SUPERHEATED_STEAM_BOILER(4,2,"Superheated Steam Boiler"), 

    //THERMOCOMPRESSOR(1,1,"Termocompressor"),

    WATER_TENDERS(2,1,"Water Tenders"), 

    WALES_CONCESSION(4,1,"Wales Concession");

    int amount; //amount of technology cards
    int loca; //amount of locamotives needed to purchase a tech card
    String name;

    /**
     *Constructor for technology enum
     *@param amount the amount of tech cards
     *@param l amount of locamotives it costs
     *@param name the name of the tech card
     */
    Technology( int amount, int l, String name){
        this.name=name;
        this.amount=amount;
        loca = l;

    }

    /**
     *Getter method to get the String as a name 
     *@return name the name of the card
     */
    public String getName(){
        return name;
    }

    /**
     *Getter method to get the amount of tech cards left 
     *@return amount the amount of tech card
     */
    public int getAmount(){
        return amount;
    }

    /**
     *Method that reduces the amount of tech cards 
     */
    public void reduceAmount(){
        amount--;
    }

    /**
     * Method to get the cost of each tech card
     * @return loca the cost of tech card
     */
    public int getCost() {
        return loca;
    }
}
