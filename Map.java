import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;
/**
 * This class deals with the aspect of the Map component of the applet. 
 * 
 * @author (Anthony, Rob, Jen, Tom, Matt) 
 * @version (Ticket to Ride)
 */
public class Map extends JPanel implements MouseListener, MouseMotionListener
{
    private Image gameBoard;
    Dimension dim;
    Image masterPic;
    Destination[] destCard;
    Locations[] locations;
    Player current;
    Train trainCard;
    /**
     * Called by the browser or applet viewer to inform this JApplet that it
     * has been loaded into the system. It is always called before the first 
     * time that the start method is called.
     */
    public Map(){
        locations=Locations.values();
        addMouseListener(this);
        addMouseMotionListener(this);
        try {
            gameBoard = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\final_map.jpg"));
        } catch (IOException e) {
            System.err.println("File Not Found, Blame Jen");
        }
        try {
            masterPic = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\Metal.jpg"));
        } catch (IOException e) {
            System.err.println("File Not Found, Blame Jen");
        }

    }

    /**
     * This method updates to the board what player's turn it 
     * currently is. This includes the player's current hand
     * for Destination, Train,  and Technology Cards, as well 
     * as the player photo. 
     * @param i - an object of the player class
     */
    public void update(Player i){
        current=i;
        repaint();
    }

    Locations selected;
    Locations selected2;

    /**
     * This method is activated when the user presses the mouse button. 
     * The player is able to hover over cities, and capture routes
     * if all of the credientials are met. 
     * @param e - a mouseEvent that is occuring 
     */ public void mousePressed(MouseEvent e){
        if(!current.lockFromCap&&!current.prevCapRoute){
            if(e.getButton()==MouseEvent.BUTTON1){
                if(hover!=null&&selected==null){
                    selected=hover;
                }
                else if(hover2!=null&&hover2!=hover){
                    selected2=hover2;
                    if(Driver.nodes.addPath(current,hover,hover2)){
                        current.prevCapRoute=true;
                        current.lockFromCap=true;
                        repaint();
                        Driver.left.update(current);
                        selected=null;
                        hover=null;
                        hover2=null;
                        selected2=null;
                        current.lockFromCap= current.prevCapRoute= true;
                        pathSync();
                    }
                }
            }
            else if(e.getButton()==MouseEvent.BUTTON3){
                selected=null;
                selected2=null;
                hover=null;
                hover2=null;
            }
        }
        else{
            error("You Can No Longer Capture A Route");
            selected=null;
            selected2=null;
            hover=null;
            hover2=null;
        }
    }

    /**
     * This method adds the appropriate amount of points when a route 
     * is captured. 
     */
    public synchronized void pathSync(){
        for(int i=0;i<current.getDestinationCard().size();i++){
            Destination d=current.getDestinationCard().get(i);
            boolean wasPath=current.isPathStart(d.start,d.end);
            if(d.end2!=null){
                wasPath=current.isPathStart(d.start,d.end2);
            }
            if(wasPath){
                current.addPoints(d.points);
                current.removeDestinationCard(d);
            }
        }
    }

    /**
     * This method is used to tell the player that they have made
     * an illegal move. 
     * @param s - a String that is displayed in the MessageDialog.
     */
    public void error(String s){
        JOptionPane.showMessageDialog(
            null,s,"Sorry :(",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Invoked when a mouse button as been clicked (pressed and released)
     * on a component. 
     * @param e - a mouseEvent that is occuring. 
     */
    public void mouseClicked(MouseEvent e){}

    /**
     * Invoked when the mouse enters a component.
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseEntered(MouseEvent e){}

    /**
     * Invoked when a mouse button button has been released 
     * on a component. 
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseReleased(MouseEvent e){}

    /**
     * Invoked when the mouse exits a component.
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseExited(MouseEvent e){}

    /**
     * Invoked when a mouse has been dragged in a component. 
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseDragged(MouseEvent e){}

    Locations hover;
    Locations hover2; 
    String hov;
    String hov2; 
    int mouseX;
    int mouseY;

    /**
     * Invoked when a mouse is moved in a componenet. 
     * @param e - a mouseEvent that is occuring. 
     */
    public void mouseMoved(MouseEvent e){
        //determines if you can over over cities after starting
        // to capture a route. 
        if(!current.lockFromCap&&!current.prevCapRoute){
            if(selected==null){
                for(Locations loc: locations){
                    if(e.getX()>=loc.x-25&&e.getX()<=loc.x+25
                    &&e.getY()>=loc.y-25&&e.getY()<=loc.y+25){
                        hover=loc;
                        updateHoverString(hover);
                        mouseX = e.getX();
                        mouseY = e.getY();
                    }
                }
            }
            else{
                for(Locations loc: locations){
                    if(e.getX()>=loc.x-25&&e.getX()<=loc.x+25
                    &&e.getY()>=loc.y-25&&e.getY()<=loc.y+25){
                        hover2=loc;
                        updateHover2String(hover2);
                        mouseX = e.getX();
                        mouseY = e.getY();
                    }
                }
            }
        }
        else{
            selected=null;
            selected2=null;
            hover=null;
            hover2=null;
        }
    }

    /**
     * Displays what city the mouse is currently hovering over on the map.
     * This is locked once the user clicks on the starting city of the route
     * they want to capture. 

     * @param update - an object of the Locations class
     */public void updateHoverString(Locations update) {
        hov = update + "";
        hov = hov.charAt(0) + (hov.substring(1).toLowerCase() );
        int spot=hov.indexOf("_");
        if(spot!=-1){
            String a=hov.substring(0,spot);
            hov=a+" "+hov.substring(
                spot+1,spot+2).toUpperCase()+hov.substring(spot+2);
        }
    }

    /**
     *  Displays what city the mouse is currently hovering over on the map. 
     *  This is locked once the user clicks on the ending city of the route
     *  they want to capture. 
     *  @param update - an object of the Locations class
     */
    public void updateHover2String(Locations update) {
        hov2 = update + "";
        hov2 = hov2.charAt(0) + (hov2.substring(1).toLowerCase() );
        int spot=hov2.indexOf("_");
        if(spot!=-1){
            String a=hov2.substring(0,spot);
            hov2=a+" "+hov2.substring(
                spot+1,spot+2).toUpperCase()+hov2.substring(spot+2);
        }
    }

    /**
     * Used for debugging purposes to display a desired string of 
     * information pertinant to what is being debugged. 
     * @param s - a String object with desired debugging information
     */
    public void debug(String s){
        JOptionPane.showMessageDialog(
            null,s,"Debugging String",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Mutator method for the size of the applet 
     * Sets the size of the applet to a specified dimension. 
     * @param d - a dimension of the applet. 
     */
    public void setSize(Dimension d){
        dim=d;
    }

    /**
     * Accessor method for the size of the applet. 
     * @return - returns the size of the applet.    
     */
    public Dimension getSize(){
        return dim;
    }

    /**
     * Paint method for applet. This method dispays the map component 
     * of the applet and all of its wonders. 
     * @param  g - the Graphics object for this applet
     */
    public void paintComponent(Graphics g)
    {

        g.drawImage(masterPic,0,0,getWidth(),getHeight(),this);
        g.drawImage(gameBoard,0,25,this);
        g.setColor(new Color(100,00,160,100));
        Font display = new Font("Serif", Font.BOLD, 20);
        Font myFont = new Font ("Courier New",Font.BOLD, 12);
        for(Player p: Driver.players){
            if(p.getName().equals("True God")){
                if (p.getPlayerNumber()  == 1){
                    g.setFont(myFont);
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,65 );
                    g.drawString("Points:"  , 110, 65);
                    g.setFont(display);
                    g.drawString(" ∞ ", 160, 65);
                }
                else if (p.getPlayerNumber() == 2){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,85 );
                    g.drawString("Points: ", 110, 85);
                    g.setFont(display);
                    g.drawString(" ∞ ", 160, 85);
                }
                else if (p.getPlayerNumber() == 3){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,105 );
                    g.drawString("Points: " , 110, 105);
                    g.setFont(display);
                    g.drawString(" ∞ ", 160, 105);
                }
                else if (p.getPlayerNumber() == 4){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,125 );
                    g.drawString("Points: " , 110, 125);
                    g.setFont(display);
                    g.drawString(" ∞ ", 160, 125);
                }
            }
            else{
                g.setFont(myFont);
                if (p.getPlayerNumber()  == 1){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,65 );
                    g.drawString("Points: " + (
                            p.getPoints() ), 110, 65);
                }
                else if (p.getPlayerNumber() == 2){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,85 );
                    g.drawString("Points: " + (
                            p.getPoints() ), 110, 85);
                }
                else if (p.getPlayerNumber() == 3){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,105 );
                    g.drawString("Points: " + (
                            p.getPoints() ), 110, 105);
                }
                else if (p.getPlayerNumber() == 4){
                    g.drawString("Player: " + (
                            p.getPlayerNumber()), 25,125 );
                    g.drawString("Points: " + (
                            p.getPoints() ), 110, 125);
                }
            }
        }

        for(Player p: Driver.players){
            for(LinkedListOfCities.Path edges:p.getControlledPaths()){
                if(edges==null){continue;}
                if(edges.x==null){continue;}
                g.setColor(p.getColor());
                int[] xArray=new int[4];
                int[] yArray=new int[4];
                for(int i=0;i+3<edges.x.size();i+=4){
                    for(int j=0;j<4;j++){
                        xArray[j]=edges.x.get(i+j);
                        yArray[j]=edges.y.get(i+j);
                    }
                    g.fillPolygon(xArray,yArray,4);
                }
            }
        }
        if(hover!=null){
            g.setColor(new Color(160,00,00,100));
            g.fillOval(hover.x-25,hover.y-25,50,50);
            if(hover2==null){
                g.setColor(Color.BLACK);
                g.setFont(display);
                g.drawString(hov, mouseX, mouseY);
            }
        }
        if(hover2!=null){
            g.setColor(new Color(00,00,160,100) );
            g.fillOval(hover2.x-25,hover2.y-25,50,50);
            g.setColor(Color.BLACK);
            g.setFont(display);
            g.drawString(hov2, mouseX, mouseY);
        }
    }
}

