import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;
/**
 * This class deals with the aspect of the RightPanel component of the applet. 
 * 
 * @author (Anthony, Rob, Jen, Tom, Matt) 
 * @version (Ticket to Ride)
 */
public class RightPanel extends JPanel implements MouseListener
{
    private Image masterPic;
    private Image trainCard;
    private Image destCard;
    private Image techCard;    
    private Image instruct;
    Dimension dim;
    private Image endTurn;
    private Player current;
    private String g = (
            ".\\Ticket to Ride Photos\\Destination cards\\destination card.jpg"
        );
    /**
     * Called by the browser or applet viewer to inform this JApplet that it
     * has been loaded into the system. It is always called before the first 
     * time that the start method is called.
     */
    public RightPanel()
    {
        addMouseListener(this);
        try {
            masterPic = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\Metal.jpg"));          
            instruct = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\instruct.png"));
            trainCard = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\Train cards\\train card.jpg"));
            destCard  = ImageIO.read(new File(g));
            techCard = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\Technologies\\technology.png"));
            endTurn = ImageIO.read(new File(
                    ".\\Ticket to Ride Photos\\endTurn.png"));
        } catch (IOException e) {
            System.err.println("File Not Found Blame Jen");
        }
        dim=new Dimension(400,500);
        drawCards();
    }
    //click within the points
    int n;
    /**
     * This method is activated when the user presses the mouse button.
     * The player is able to hover over cities, and capture routes
     * if all of the credientials are met. 
     * @param e - a mouseEvent that is occuring 
     */ 
    public void mousePressed(MouseEvent e){
        if(e.getButton()==MouseEvent.BUTTON1){
            int x=e.getX();
            int y=e.getY();
            int k=withinPlay(x,y);
            //debug("X = "+e.getX()+", Y = "+e.getY());
            boolean hasWaterTenders=current.getOwnedTech().contains(
                    Technology.WATER_TENDERS);
            int maxCardPick=hasWaterTenders?3:2;
            if( withinTrainDeck( x, y )) {
                Driver.deck.restack();
                if(hasWaterTenders||current.pickupCounter<2){
                    if(!current.prevCapRoute && (current.pickupCounter<3) 
                    && !current.lockTrain){
                        current.prevBoughtTech = true;
                        current.lockDest=true;
                        current.lockFromCap = true;
                        current.switchMethod(1,Driver.deck.getTop());
                        current.pickupCounter++;
                    }else{
                        Driver.map.error("No More Pick-ups Allowed");
                    }
                }
            }
            else if( withinDestDeck( x, y )&& !current.lockDest  
            && !current.prevCapRoute) {
                if (Driver.deck.destinationCard.size() == 0 ) {
                    Driver.map.error(
                        "There are no more cards in the destination deck");
                    return;
                }
                ArrayList<Destination> selectable=Driver.deck.destSelection();
                ArrayList<ImageIcon> selectableImage= Driver.deck.toImage(
                        selectable.toArray(new Destination[selectable.size()]));
                boolean hitCancel=false;
                JFrame choose=new JFrame();
                int count=0;
                while(count==0||!hitCancel){
                    hitCancel=false;
                    ImageIcon[] arr=new ImageIcon[selectable.size()];
                    arr=selectableImage.toArray(arr);

                    int index= JOptionPane.showOptionDialog(choose,
                            "How many Desticnaiton Cards will you be purchasing today?",
                            "Destination Select",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            arr, //array
                            arr[0]);
                    if(index==arr.length-1){
                        hitCancel=true;
                    }
                    else if(index==-1){continue;}
                    else{
                        count++;
                        current.addDestinationCard(selectable.get(index));
                        selectable.remove(index);
                        selectableImage.remove(index);
                        current.prevBoughtTech = true;
                        current.lockDest = true;
                        current.lockFromCap = true;
                        current.lockTrain=true;
                    }
                }
            }else if (withinInstruct(x,y) ) {
                notice("Ticket to Ride Instructions \n \n"
                    + " Players may preform one of 4 actions per turn \n \n"
                    + " Those actions are as follows: \n \n" 
                    + " 1: Buy a technology card at beginnig of turn \n"
                    + " *if you do any other actions first, you will "
                    + " not be able to buy a technology card that turn* \n \n"
                    + " 2. Draw two train cards from the train deck OR "
                    + " from the five face-up cards, or a mix of the two \n \n"
                    + " 3. Draw 3 destination tickets and choose at least one,"
                    + " up to three. \n \n"
                    + " 4. Capture a route on the board \n \n"
                    + " Interacting with the board: \n \n"
                    + " \t 0.) Selecting a card \n "
                    + " \t\t On the left side the current players hand is "
                    + " displayed. Left click on a card to select it, right "
                    + " click to deselect. The counters in the black boxes"
                    + " will update acorrdingly \n"
                    + " \t\t *You will need to select a card value, or a" 
                    + " combination of cards > or = to the amount"
                    + " of cards necessary to perform an action \n "
                    + " \t 1.) Buy Technologies \n" 
                    + " \t\t To buy technologies, left click on locomotives"
                    + " in the hand and left click on the technology button"
                    + " of Lim on the right hand side \n \n"
                    + " \t 2.) Draw Train Cards \n"
                    + " \t\t To draw train cards, left click either the train "
                    + " card deck or the face-up train cards \n \n"
                    + " \t 3.) Draw a Destination Card \n"
                    + " \t\t To draw destination cards, left click onto the "
                    + "destination card deck, in a new window the player may"
                    + " choose one by left-clicking the desired card \n \n"
                    + " \t 4.) Claim a Route \n"
                    + " \t\t To claim a route, left click on the train cards "
                    + "in the current player's hand that are needed to claim"
                    + " the route, \n"
                    + " \t\t left click onto one city and left click on "
                    + " another adjacent city \n"
                    + " \t\t If the player can claim that route then the path "
                    + "will be overwritten in that player's color \n \n " 
                    + " \t 5.) Convert to Locomotives \n"
                    + " \t\t To convert train cards to locomotives, left click"
                    + " on the cards the current player would like to get ride"
                    + "of and left click the Convert To: button \n"
                    + " \t\t *If you want to undo selecting from the hand, then"
                    + " right click each card \n"
                    + " \t\t *Please note that at any time during his or her"
                    + " turn, the current player may convert train cards to "
                    + "locomotives \n \n "
                    + " \t 6.) View Current Player Technologies and"
                    + " Destination Cards \n"
                    + " \t\t To view the current player's destination cards," 
                    + "then left click on the My Destinations icon to "
                    + "cycle through the owned technologies \n"
                    + " \t\t To view the current player's technologies, " 
                    + "then left"
                    + " click on the My Technology icon \n \n"
                    + " *At the end of their turn, the player left clicks "
                    + " the end turn icon. The player will be locked out "
                    + "of doing activities"
                    + " that would consist of a turn ");
            }
            else if( withinTechButton( x, y )&& !current.prevBoughtTech  
            && !current.prevCapRoute) {
                ArrayList<Technology> temp=Driver.deck.technologyCard;
                String[] fullString = new String[temp.size()];
                for(int i=0; i<temp.size();i++){
                    fullString[i] = temp.get(i).getName(
                    ) + " Cost " + temp.get(i).getCost() + " Locamotives";
                } 
                String s = (
                    String)JOptionPane.showInputDialog(null, 
                        "Choose Technology",
                        null, JOptionPane.QUESTION_MESSAGE,
                        null, fullString, fullString[0]);
                if (s == null) return;
                int index =0;
                for (int i = 0; i < fullString.length; i++) {
                    if (s.equals(fullString[i] ) ) {
                        index = i;
                    }
                }

                if (current.trainSelectCount[8] < temp.get(index).getCost(
                ) ) {
                    Driver.map.error(" Insufficient Locomotive Cards");
                    return;
                } 
                current.prevBoughtTech = true;
                for ( int i = 0; i < temp.get(index).getCost(); i++) {
                    current.trainSelectCount[8]--;
                    Driver.deck.discardPile.add(Train.LOCAMOTIVE);
                }

                int left;
                temp.get(index).reduceAmount();
                current.addTechCards( temp.get(index) );
                left = temp.get(index).getAmount();
                if (left == 0){
                    temp.remove(temp.indexOf(index));
                    JOptionPane.showMessageDialog(this,"Out of "+temp.get(
                            index).getName(),
                        "That was the last card",
                        JOptionPane.INFORMATION_MESSAGE);
                }
                else if (left>0){ 
                    JOptionPane.showMessageDialog(this,temp.get(
                            index).getName(),
                        left +" cards left",
                        JOptionPane.INFORMATION_MESSAGE);
                }
            }else if( k>=0 ){
                Driver.deck.restack();
                if(!current.prevCapRoute&&current.pickupCounter<2
                && !current.lockTrain){
                    current.prevBoughtTech = true;
                    current.lockDest = true;
                    current.lockFromCap = true;
                    current.switchMethod(1,inPlay[k]);
                    inPlay[k]=null;
                    current.pickupCounter++;
                    if(!Driver.deck.isStackEmpty()){
                        inPlay[k]=Driver.deck.getTop();
                    }
                    else if(Driver.deck.trainCards.size()<=5){
                        Driver.deck.restack();
                    }
                }
                else{
                    Driver.map.error("No More Pick-ups Left");
                }
            }
            else if (withinEndTurn(x, y) ) {
                current.isTurn();
            }
        }else if(e.getButton()==MouseEvent.BUTTON3){}
        repaint();
        Driver.left.repaint();
    }

    /**
     * This method updates to the board what player's turn it 
     * currently is. This includes the player's current hand
     * for Destination, Train,  and Technology Cards, as well 
     * as the player photo. 
     * @param i - an object of the player class
     */
    public void update(Player i){
        current=i;
        Driver.deck.restack();
        if(inPlay[0] == null && !Driver.deck.isStackEmpty()){
            inPlay[0]=Driver.deck.getTop();
        }
        if(inPlay[1] == null && !Driver.deck.isStackEmpty()){
            inPlay[1]=Driver.deck.getTop();
        }
        if(inPlay[2] == null && !Driver.deck.isStackEmpty() ){
            inPlay[2]=Driver.deck.getTop();
        }
        if(inPlay[3] == null && !Driver.deck.isStackEmpty()){
            inPlay[3]=Driver.deck.getTop();
        }
        if(inPlay[4] == null && !Driver.deck.isStackEmpty()){
            inPlay[4]=Driver.deck.getTop();
        }
        repaint();
    }

    /**
     * Detects if the mouse's x,y location is within the "End Turn" button.
     * @param x - an integer x coordinate value.
     * @param y - an integer y coordinate value.
     * @return returns true if the x,y coordiate is within the end button range,
     * false otherwise. 
     */
    public boolean withinEndTurn(int x, int y) {
        if(x > 20 && x < 100 && y > 300 && y < 400) {
            return true;
        }
        return false;
    }

    /**
     * Detects if the mouse's x,y location is within the card selection pool
     * There are 5 face down cards to choose from. 
     * @param x - an integer x coordinate value.
     * @param y - an integer y coordinate value.
     * @return returns an int value depending on which card location. 
     * the player selected from the train card pool. 
     */
    public int withinPlay(int x, int y){
        if(iPX<=x&&x<=iPXS*2){
            if(iPY<=y&&y<=iPY+iPYS){
                return 0;
            }
            else if(iPY+iPYS*1<=y&&y<=iPY+iPYS*1+iPYS){
                return 1;
            }
            else if(iPY+iPYS*2<=y&&y<=iPY+iPYS*2+iPYS){
                return 2;
            }
            else if(iPY+iPYS*3<=y&&y<=iPY+iPYS*3+iPYS){
                return 3;
            }
            else if(iPY+iPYS*4<=y&&y<=iPY+iPYS*4+iPYS){
                return 4;
            }
        }
        return -1;
    }

    Train[] inPlay=new Train[5];
    /**
     * Generates 5 random train cards from the train card 
     * deck, selecting from the top of the deck. 
     */
    public void drawCards(){
        if(inPlay[0]==null){
            inPlay[0]=Driver.deck.getTop();
        }
        if(inPlay[1]==null){
            inPlay[1]=Driver.deck.getTop();
        }
        if(inPlay[2]==null){
            inPlay[2]=Driver.deck.getTop();
        }
        if(inPlay[3]==null){
            inPlay[3]=Driver.deck.getTop();
        }
        if(inPlay[4]==null){
            inPlay[4]=Driver.deck.getTop();
        }
    }

    /**
     * Determines if the mouse is within range of the 
     * train deck. 
     * @param x - an integer x coordinate value
     * @param y - an integer y coordinate value
     * @return returns true if the mouse is within range
     * of the train deck, and false otherwise 
     **/
    public boolean withinTrainDeck(int x, int y) {
        return (x > 175 && x < 305 && y > 24 && y < 195);
    }

    /**
     * Determines if the mouse is within range of the 
     * destination deck. 
     * @param x - an integer x coordinate value
     * @param y - an integer y coordinate value
     * @return returns true if the mouse is within range
     * of the destination deck, and false otherwise 
     **/
    public boolean withinDestDeck(int x, int y) {
        return (x > 25 && x < 154 && y > 25 && y < 195);
    }

    /**
     * Determines if the mouse is within range of the 
     * technology button. 
     * @param x - an integer x coordinate value
     * @param y - an integer y coordinate value
     * @return returns true if the mouse is within range
     * of the technology button, and false otherwise 
     **/
    public boolean withinTechButton(int x, int y) {
        return (x > 19 && x < 120 && y > 836 && y < 930);
    }

    /**
     * Determines if the mouse is within range of the 
     * instructions button. 
     * @param x - an integer x coordinate value
     * @param y - an integer y coordinate value
     * @return returns true if the mouse is within range
     * of the instructions button, and false otherwise 
     **/
    public boolean withinInstruct(int x, int y) {
        return ( x > 36 && x < 109 && y > 550 && y < 642  ) ;
    }

    /**
     * Invoked when a mouse button as been clicked (pressed and released)
     * on a component. 
     * @param e - a mouseEvent that is occuring. 
     */
    public void mouseClicked(MouseEvent e){}

    /**
     * Invoked when the mouse enters a component.
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseEntered(MouseEvent e){}

    /**
     * Invoked when a mouse button button has been released 
     * on a component. 
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseReleased(MouseEvent e){}

    /**
     * Invoked when the mouse exits a component.
     * @param e - a mouseEvent that is occuring.
     */
    public void mouseExited(MouseEvent e){}

    /**
     * Mutator method for the size of the applet 
     * Sets the size of the applet to a specified dimension. 
     * @param d - a dimension of the applet. 
     */
    public void setSize(Dimension d){
        dim=d;
    }

    /**
     * Used for debugging purposes to display a desired string of 
     * information pertinant to what is being debugged. 
     * @param s - a String object with desired debugging information
     */
    public void debug(String s){
        JOptionPane.showMessageDialog(
            this,s,"Debugging String",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Used when the user clicks on the instructions button. 
     * Displays a messageDialog of quick instructions on how to
     * play the game. 
     * @param s - a String of instructions 
     */
    public void notice(String s){
        JOptionPane.showMessageDialog(
            this,s,"How to play",JOptionPane.INFORMATION_MESSAGE);
    }

    final int iPX=150;  //inPlayX
    final int iPY=230; //inPlayY
    final int iPXS=174;//inPlayXScale
    final int iPYS=132;//inPlayYScale
    final int sF=100;  //shiftFactor
    Font display3 = new Font("Serif", Font.BOLD, 20);
    /**
     * Paint method for applet. This method dispays the map component 
     * of the applet and all of its wonders. 
     * @param  g - the Graphics object for this applet
     */
    public void paintComponent(Graphics g)
    {
        g.drawImage(masterPic,0,0,getWidth(),getHeight(),this);
        g.drawImage(trainCard,175,25,132,174,this);        
        g.drawImage(destCard,25,25,132,174,this);
        g.drawImage(techCard,20,835,100,102,this);
        g.drawImage(endTurn,20,300,100,100,this);
        g.drawImage(instruct,22,550,100,100,this);

        g.setFont(display3);
        g.setColor(current.getColor());
        g.drawString("Instructions", 22,540);
        g.drawString("Buy Destinations", 20,20);
        g.drawString("Train Deck", 190,20);
        g.drawString("Buy Technologies", 0,830);
        if(inPlay[0]!=null){
            g.drawImage(Driver.deck.getTrainImage(
                    inPlay[0]),iPX,iPY,iPXS,iPYS,this);
        }
        if(inPlay[1]!=null){
            g.drawImage(Driver.deck.getTrainImage(
                    inPlay[1]),iPX,iPY+iPYS,iPXS,iPYS,this);
        }
        if(inPlay[2]!=null){
            g.drawImage(Driver.deck.getTrainImage(
                    inPlay[2]),iPX,iPY+iPYS*2,iPXS,iPYS,this);
        }   
        if(inPlay[3]!=null){
            g.drawImage(Driver.deck.getTrainImage(
                    inPlay[3]),iPX,iPY+iPYS*3,iPXS,iPYS,this);
        }
        if(inPlay[4]!=null){
            g.drawImage(Driver.deck.getTrainImage(
                    inPlay[4]),iPX,iPY+iPYS*4,iPXS,iPYS,this);
        }
    }
}