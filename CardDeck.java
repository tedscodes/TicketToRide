import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.imageio.*;
/**
 * This class is dedicated to storing and maintinging the information 
 * concerning all cards that are not in player hands.
 * 
 * @author Team Ticket To Snuffles
 * @version Last
 */
public class CardDeck 
{
    //deck with all the train cards
    public ArrayList<Train> trainCards = new ArrayList<>();
    //deck holding all the cards used
    public ArrayList<Train> discardPile = new ArrayList<>(); 
    public ArrayList<Destination> destinationCard = new ArrayList<>();
    public ArrayList<Locations> locationV = new ArrayList<>();
    public ArrayList<Technology> technologyCard = new ArrayList<>();
    Random r = new Random();//random for shuffling purposes
    Image pictureRED,pictureBLUE,pictureGREEN,pictureORANGE,pictureWHITE,
    pictureBLACK,picturePINK,pictureYELLOW,pictureLOCA,cancel;
    /**
     *Constructor of CardDeck, fills up trainCard in psuedo random order 
     */
    public CardDeck(int n){
        System.gc();
        Collections.addAll(destinationCard,Destination.values());
        Collections.addAll(technologyCard,Technology.values());
        Collections.addAll(locationV,Locations.values());
        //counter to make sure cards get distributed evenly
        int[] array= new int[]{12,12,12,12,12,12,12,12,14-n};
        while(trainCards.size()<110-n){
            int count = r.nextInt(9); //randomizer
            if(array[count]> 0){ 
                //check to see if there are still cards of certain color 
                //still available 
                switch(count){
                    case 0:
                    array[0]--;
                    trainCards.add(Train.PINK); //pink card
                    break;
                    case 1:
                    array[1]--;
                    trainCards.add(Train.WHITE); //white card
                    break;
                    case 2:
                    array[2]--;
                    trainCards.add(Train.BLUE); //blue card
                    break;
                    case 3:
                    array[3]--;
                    trainCards.add(Train.YELLOW); //yellow card
                    break;
                    case 4:
                    array[4]--;
                    trainCards.add(Train.ORANGE);//orange card
                    break;
                    case 5:
                    array[5]--;
                    trainCards.add(Train.BLACK);//black card
                    break;
                    case 6:
                    array[6]--;
                    trainCards.add(Train.RED);//red card
                    break;
                    case 7:
                    array[7]--;
                    trainCards.add(Train.GREEN);//green card
                    break;
                    case 8:
                    array[8]--;
                    trainCards.add(Train.LOCAMOTIVE);//locomotive card
                    break;
                }
            }
        }
        try{
            pictureRED = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\red train.jpg"));
            pictureBLUE = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\blue train.jpg"));
            pictureGREEN = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\green train.jpg"));
            pictureORANGE = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\orange train.jpg"));
            pictureWHITE = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\white train.jpg"));
            pictureBLACK = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\black train.jpg"));
            picturePINK = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\pink train.jpg"));
            pictureYELLOW = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\yellow train.jpg"));
            pictureLOCA = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"Train cards\\locamotive.jpg"));
            cancel = ImageIO.read(new File(".\\Ticket to Ride Photos\\"
                    +"cancel.jpg"));
        }
        catch(Exception e){
            System.err.println("Blame Obama");
        }
    }

    /**
     * This method returns a train card from the top of the 
     * train card deck
     * @return the train card at the top of the deck
     */
    public Train getTop(){
        Train t = trainCards.get(0);
        trainCards.remove(0);
        return t;
    }

    /**
     *Method to check and see if the train card deck is empty
     *@return true if empty
     */
    public boolean isStackEmpty(){
        return trainCards.size() == 0;
    }

    /**
     * This method is responsible for removing Locamotives from the deck
     * for converting purposes
     */
    public void removeLocamotive() {
        int index = trainCards.lastIndexOf(Train.LOCAMOTIVE);
        trainCards.remove(index);
    }

    /**
     * Method to add discarded cards back into the deck
     */
    public void restack(){
        if (isStackEmpty() ) {
            Collections.shuffle ( discardPile );
            trainCards.addAll( discardPile );
            discardPile.clear();
        }
    }

    /**
     * This method will present the 3 destination cards to choose from
     * @return pickOne - an array of 3 destination cards from the top fo the 
     *  pile
     */
    public ArrayList<Destination> destSelection() {
        if (Driver.deck.destinationCard.size() == 0 ) {
            return null;
        }
        int next=r.nextInt(destinationCard.size());
        ArrayList<Destination>pickOne = new ArrayList<>(5);
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        if (Driver.deck.destinationCard.size() == 0 ) {
            return pickOne ;
        }
        next=r.nextInt(destinationCard.size());
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        if (Driver.deck.destinationCard.size() == 0 ) {
            return pickOne;
        }
        next=r.nextInt(destinationCard.size());
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        return pickOne;
    }

    /**
     * This method is used to collect random cards from the destination deck
     * @return ArrayList<Destination> selection of the cards
     */
    public ArrayList<Destination> destStartSelection() {
        int next=r.nextInt(destinationCard.size());
        ArrayList<Destination>pickOne = new ArrayList<>(5);
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        next=r.nextInt(destinationCard.size());
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        next=r.nextInt(destinationCard.size());
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        next=r.nextInt(destinationCard.size());
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        next=r.nextInt(destinationCard.size());
        pickOne.add(Driver.deck.destinationCard.get(next));
        Driver.deck.destinationCard.remove(next);
        next=r.nextInt(destinationCard.size());

        return pickOne;
    }

    /**
     * This method returns the Destination cards' image as a ImageIcon for 
     * JFrames
     * @param arr Destination[] an array of the destinaitons being referenced
     * @return ArrayList<ImageIcon> of the Destinations
     */
    public ArrayList<ImageIcon> toImage( Destination[] arr){
        ArrayList<ImageIcon> temp=new ArrayList<>(arr.length+1);
        for(int i=0;i<arr.length;i++){
            Image temp2=resizeToBig(arr[i].getImage(),132,174);
            temp.add(new ImageIcon(temp2));
        }
        Image temp2=resizeToBig(cancel,132,60);
        temp.add(new ImageIcon(temp2));
        return temp;
    }

    /**
     * This method adds the 2 cards not selected to the bottom of the deck
     * @param r - a destination card array to go to the bottom of the deck
     */
    public void destReturn( Destination one, Destination two ) {
        destinationCard.add(one);
        destinationCard.add(two);
    }

    /**
     * This method is used to add one of each Technology to Player hand
     * @return Technology[] of each Technology
     */
    public Technology[] getTechAll(){
        Technology[] temp=new Technology[technologyCard.size()];
        Technology type;
        for(int adj=0,i=0;adj<technologyCard.size();i++,adj++){
            if(technologyCard.get(adj).getAmount()==1){
                type = technologyCard.get(adj);
                technologyCard.remove(adj);
                temp[i]=type;
                adj--;
            }
            else{
                type = technologyCard.get(adj);
                type.reduceAmount();
                temp[i]=type;
            }
        }
        return temp;
    }

    /**
     * This method returns a Technology and removes it from the deck
     * @param needed Technology 
     * @return Technology 
     */
    public Technology getTech(Technology needed){
        Technology type=null;
        int index=technologyCard.indexOf(needed);
        if(index>-1){
            if(technologyCard.get(index).getAmount()==1){
                type = technologyCard.get(index);
                technologyCard.remove(index);
            }
            else{
                type = technologyCard.get(index);
                type.reduceAmount();
            }
        }
        return type;
    }

    /**
     * This method returns the picture of a given train
     * @param color Train 
     * @return Image of train
     */
    public Image getTrainImage(Train color){
        switch(color){
            case RED:
            return pictureRED;
            case BLUE:
            return pictureBLUE;
            case GREEN:
            return pictureGREEN;
            case ORANGE:
            return pictureORANGE;
            case WHITE:
            return pictureWHITE;
            case BLACK:
            return pictureBLACK;
            case PINK:
            return picturePINK;
            case YELLOW:
            return pictureYELLOW;
            case LOCAMOTIVE:
            return pictureLOCA;
            default:
            return null;
        }
    }

    /**
     * This method resizes Image datatypes
     * @param originalImage Image that is being resized
     * @param biggerWidth   width to resize into
     * @param biggerHeight  height to resize into
     * @return return resized Image
     */
    public Image resizeToBig(Image originalImage, int biggerWidth, 
    int biggerHeight) {
        int type = BufferedImage.TYPE_INT_ARGB;

        BufferedImage resizedImage = new BufferedImage(biggerWidth,
                biggerHeight, type);
        Graphics2D g = resizedImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
            RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, 
            RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
            RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(originalImage, 0, 0, biggerWidth, biggerHeight, null);
        g.dispose();

        return resizedImage;
    }
}