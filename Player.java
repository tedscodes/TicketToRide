import java.util.*;
import java.awt.*;
import java.io.*;
import javax.imageio.*;
/**
 * This class deals with the aspect of player objects
 * 
 * @author (Anthony, Rob, Jen, Tom, Matt) 
 * @version (Ticket to Ride)
 */

public class Player
{
    private ArrayList<LinkedListOfCities.Path> controlledPaths =
        new ArrayList<>();//change to paths
    private ArrayList<Technology> ownedTech = 
        new ArrayList<>(); // change to technologies
    private ArrayList<Destination> destinationCard =
        new ArrayList<>();
    public int[] trainCount = new int[9];
    public int[] trainSelectCount = new int[9];
    private boolean isTurn;
    private int playerNumber;
    public Image masterPic;
    private Color playerColor;
    private int points;
    private int piecesLeft;
    private String name;
    public boolean prevBoughtTech = false;
    public boolean prevCapRoute = false;
    public boolean lockFromCap = false;
    public boolean lockDest;
    public boolean lockTrain;
    public int pickupCounter =0;

    /**
     * Constructor for player objects
     * @param pNum - the number of the player, this 
     *      also sets the starting points for the player
     * @param c - This sets the color for the player
     * @param pic - This sets the picture for the player
     */
    public Player(int pNum, Color c, String name){
        this.name=name;
        isTurn=false;
        playerNumber = points = pNum+1;
        playerColor = c;
        piecesLeft = 35;
        for(int i=0;i<4;i++){
            Train temp=Driver.deck.getTop();
            switchMethod(1,temp);
        }
        switchMethod(1,Train.LOCAMOTIVE);
    }

    /**
     * This method is used to reset map and right panel values 
     * when a players turn is ended
     */
    public void isTurn(){
        isTurn=!isTurn;
        pickupCounter=0;
        prevBoughtTech=false;
        prevCapRoute = false;
        lockDest=false;
        lockFromCap = false;
        Driver.map.selected=null;
        Driver.map.selected2=null;
        Driver.map.hover=null;
        Driver.map.hover2=null;
    }

    /**
     * This method is used to get the state of the player's turn
     */
    public boolean getIsTurn(){
        return isTurn;
    }

    /**
     * This method is used to return the private variable
     * ownedTech
     * @return ArrayList<Technology>
     */
    public ArrayList<Technology> getOwnedTech() {
        return ownedTech;
    }

    /**
     * This method is used to give specific players advantages
     */
    public void headStart() {
        for(int j=0;j<40;j++)
            switchMethod(1,Driver.deck.getTop());
        Collections.addAll(ownedTech,Driver.deck.getTechAll());
    }

    /**
     * This method add cards to the players hand
     * @param cOne - the first card to add
     */
    public void addHand(Train cOne) {
        switchMethod(1,cOne);
    }

    /**
     * This method is used to add or remove trains from a player's
     * hand
     * @param add int (+ = add, - = subtract, 0 = no modification)
     * @param color Train type of train to be modified
     * @return int amount of trains left
     */
    public int switchMethod(int add, Train color){
        int adder;
        if(add>0){
            adder=1;
        }
        else if(add<0){
            adder=-1;
        }
        else{
            adder=0;
        }
        if(color == null) return -1;
        switch(color){
            case RED:
            trainCount[0]+=adder;
            return trainCount[0];
            case BLUE:
            trainCount[1]+=adder;
            return trainCount[1];
            case GREEN:
            trainCount[2]+=adder;
            return trainCount[2];
            case ORANGE:
            trainCount[3]+=adder;
            return trainCount[3];
            case WHITE:
            trainCount[4]+=adder;
            return trainCount[4];
            case BLACK:
            trainCount[5]+=adder;
            return trainCount[5];
            case PINK:
            trainCount[6]+=adder;
            return trainCount[6];
            case YELLOW:
            trainCount[7]+=adder;
            return trainCount[7];
            case LOCAMOTIVE:
            trainCount[8]+=adder;
            return trainCount[8];
            default: return -1;
        }
    }

    /**
     * This method is not used, sets the players' train hand to a 
     * given hand
     * @param arr int[] hand to be set
     */
    private void setTrainCount(int[] arr){
        trainCount=arr;
    }

    /**
     * This method is used to start the recursive search for a 
     * given path to Location goal from start
     * @param start Locations 
     * @param goal Locations
     * @return boolean
     */
    public boolean isPathStart(Locations start, Locations goal){
        boolean[] visitedCities=new boolean[57];
        for(LinkedListOfCities.Path path:
        Driver.nodes.ownedRoutesFrom(this, start)){
            if(path==null){
                continue;
            }
            else {
                int yes=Driver.deck.locationV.indexOf(start);
                visitedCities[yes]=true;
                return (isPath(path.end,goal,visitedCities));
            }
        }
        return false;
    }

    /**
     * This method is used to check if two paths are connected by
     * the player
     * @param a Locations
     * @param b Locations
     * @param visitedCities boolean[]
     * @return boolean
     */
    public boolean isPath(Locations a, Locations b,
    boolean[] visitedCities){
        int yes=Driver.deck.locationV.indexOf(a);
        visitedCities[yes]=true;
        if(a==b){return true;}
        else{
            for(LinkedListOfCities.Path path:
            Driver.nodes.ownedRoutesFrom(this, a)){
                if(path==null){
                    continue;
                }
                else {
                    int no=Driver.deck.locationV.indexOf(path.end);
                    if(!visitedCities[no]){
                        return isPath(path.end,b,visitedCities);
                    }
                }
            }
        }
        return false;
    }

    /**
     * This method returns the list of all paths
     *      controlled by the player
     * @return controlledPaths - the array list containing
     *      all paths controlled by the player
     */
    public ArrayList<LinkedListOfCities.Path> getControlledPaths() {
        return controlledPaths;
    }

    /**
     * This method updates the paths controlled by the 
     *      player
     * @param pathOne - the path to add
     */
    public void addPath(Locations a, Locations b) {
        if(!Driver.nodes.addPath(this,a,b)){
            System.out.println("Illegal Move.");
        }
    }

    /**
     * This method updates the tech card controlled by 
     *      player
     * @param tech - the tech card to be added
     */
    public void addTechCards(Technology tech) {
        ownedTech.add(tech);
    }

    /**
     * This method updates the tech card controlled by 
     *      player
     * @param techR - the tech card to be removed
     */
    public void removeTechCards(Integer techR) {
        ownedTech.remove(techR);
    }

    /**
     * This method updates the destination cards 
     *      controlled by the player
     * @param dest - the destination card to be added
     */
    public void addDestinationCard(Destination dest) {
        destinationCard.add(dest);
    }

    /**
     * This method updates the destination cards 
     *      controlled by the player
     * @param dest - the destination card to be removed
     */
    public void removeDestinationCard(Destination dest) {
        destinationCard.remove(dest);
    }

    /**
     * This method return the master image of the player
     * @return masterPic - the master image of the player
     */
    public Image getMasterPic() {
        return masterPic;
    }

    /**
     * This method returns the number of the player
     * @return playerNumber - the number of the player
     */
    public int getPlayerNumber() {
        return playerNumber;
    }

    /**
     * This is an accessor method for Destination cards
     * @return ArrayList<Destination> 
     */
    public ArrayList<Destination> getDestinationCard() {
        return destinationCard;
    }

    /**
     * This method is used to set the state of the Players turn
     */
    public void setIsTurn() {
        isTurn=true;
        prevBoughtTech = false;
        lockDest = false;
        lockTrain=false;
        lockFromCap = false;
        Driver.left.destPicCount = 0;
        Driver.left.setSelectedDestPic(this);
    }

    /**
     * This method returns the color of the player
     * @return playerColor - the color of the player
     */
    public Color getColor() {
        return playerColor;
    }

    /**
     * This method sets the color of the player
     * @param c - the color of the player
     */
    public void setColor(Color c ) {
        playerColor = c;

        try{
            if(c!=null){
                if (playerColor == Color.RED) {
                    if (this.name.equals("Lim God") ) {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"limgod.png"));
                    }
                    else {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"Player\\red.png"));
                    }
                }
                else if (playerColor == Color.GREEN ) {
                    if (this.name.equals("Lim God") ) {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"limgod.png"));
                    }
                    else {
                        this.masterPic = ImageIO.read(new File(
                                ".\\Ticket to Ride Photos\\"
                                +"Player\\green.png"));
                    }
                }
                else if (playerColor == Color.BLUE ) {
                    if (this.name.equals("Lim God") ) {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"limgod.png"));
                    }
                    else {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"Player\\blue.png"));
                    }
                }
                else if (playerColor == Color.YELLOW ) {
                    if (this.name.equals("Lim God") ) {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"limgod.png"));
                    }
                    else {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"Player\\yellow.png"));
                    }
                }
                else if (playerColor == Color.BLACK ) {
                    playerColor = Color.WHITE;
                    if (this.name.equals("Lim God") ) {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"limgod.png"));
                    }
                    else {
                        this.masterPic = ImageIO.read(
                            new File(".\\Ticket to Ride Photos\\"
                                +"Player\\black.png"));
                    }
                }
            }else{
                try{
                    name="True God";
                    points=99999999;
                    for(int i=0;i<9;i++){
                        trainCount[i]=99;
                    }
                    playerColor = Color.ORANGE;
                    this.masterPic= ImageIO.read(
                        new File(".\\Ticket to Ride Photos\\"
                            +"Player\\Spahgetti God.jpg"));
                }
                catch(Exception e){
                    System.err.println("File Not Found <Player>");
                }
            }
        }catch(Exception e){
            System.err.println("File Not Found <Player>");
        }
    }

    /**
     * This method returns the points of the player
     * @return the points of the player
     */
    public int getPoints() {
        return points;
    }

    /**
     * This method returns the player's name
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * This method updates the players points
     * @param p - the points to be added to the players
     *      point count
     */
    public void addPoints(int p) {
        points = points + p;
    }

    /**
     * This method returns the current amount of pieces a 
     * player has
     * @return piecesLeft - the amount of pieces a player
     *      has left
     */
    public int getPiecesLeft() {
        return piecesLeft;
    }

    /**
     * This method decrements the number of train cars left for
     * the player to construct
     * @param change int
     */
    public void decrementPiecesLeft(int change) {
        piecesLeft-=change;
    }

    /**
     * This method updates the amount of pieces the player has left
     * @param amt - the amount of pieces to be substracted
     */
    public void updatePieces(int amt) {
        piecesLeft = piecesLeft - amt;
    }

    /**
     * This is method adds a destination to the Player's hand 
     * @param dest Destination
     */
    public void addToDestinationCard(Destination dest) {
        destinationCard.add(dest);
    }
}