import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;
import java.awt.event.*;
import java.util.*;
// RED, BLUE, GREEN, ORANGE, WHITE, BLACK, PINK, YELLOW, LOCAMOTIVE;
/**
 * This class is dedicated to everything on the left side of the 
 * board
 * 
 * @author Team Ticket To Snuffles
 * @version Last
 */
public class LeftPanel extends JPanel implements MouseListener
{
    private Image masterPic;
    private Player current;
    private Image techCard;
    private Image selectedDestPic;
    private ArrayList<Train> currentPlayerHand = 
        new ArrayList<>();
    Train[] inThisOrder = {Train.RED, Train.BLUE, 
            Train.GREEN, Train.ORANGE,Train.WHITE, Train.BLACK, 
            Train.PINK, Train.YELLOW, Train.LOCAMOTIVE};
    Dimension dim;
    /**
     * Constructor for objects of class LeftPanel
     */
    public LeftPanel()
    {
        addMouseListener(this);
        try {
            masterPic = 
            ImageIO.read(new File(".\\Ticket to Ride Photos\\"+
                    "Metal.jpg"));
            techCard = 
            ImageIO.read(new File(".\\Ticket to Ride Photos\\"+
                    "Technologies\\technology.png"));
        } catch (IOException e) {
            System.err.println("File Not Found Blame Jen");
        }

        dim=new Dimension(400,500);
        repaint();
    }

    /**
     * This method is used to update the board and reestablish
     * the player
     * @param i Player to be used
     */
    public void update(Player i){
        current=i;
        repaint();
    }

    /**
     * This method is used to update selected cards
     * true selects, false deselects
     * @param add boolean
     * @param box int
     */
    public void findWhichCard(boolean add, int box) {
        int adder;
        if(!add){
            adder=-1;
        }
        else{
            adder=1;
        }
        try {
            Train color = inThisOrder[box];
            switch ( color ) {
                case RED :
                if (current.trainCount[0] > 0 && add==true ) {
                    current.trainCount[0]-=adder;
                    current.trainSelectCount[0]+=adder;
                }
                else if(current.trainSelectCount[0]!=0 && !add) {
                    current.trainCount[0]-=adder;
                    current.trainSelectCount[0]+=adder;
                }
                break;
                case BLUE:
                if (current.trainCount[1] != 0 && add) {
                    current.trainCount[1]-=adder;
                    current.trainSelectCount[1]+=adder;
                }
                else if(current.trainSelectCount[1]!=0&& !add) {
                    current.trainCount[1]-=adder;
                    current.trainSelectCount[1]+=adder;
                }
                break;
                case GREEN:
                if (current.trainCount[2] != 0 && add) {
                    current.trainCount[2]-=adder;
                    current.trainSelectCount[2]+=adder;
                }
                else if(current.trainSelectCount[2]!=0&& !add) {
                    current.trainCount[2]-=adder;
                    current.trainSelectCount[2]+=adder;
                }
                break;
                case ORANGE:
                if (current.trainCount[3] != 0 && add) {
                    current.trainCount[3]-=adder;
                    current.trainSelectCount[3]+=adder;
                }
                else if(current.trainSelectCount[3]!=0&& !add) {
                    current.trainCount[3]-=adder;
                    current.trainSelectCount[3]+=adder;
                }
                break;
                case WHITE:
                if (current.trainCount[4] != 0 && add) {
                    current.trainCount[4]-=adder;
                    current.trainSelectCount[4]+=adder;
                }
                else if(current.trainSelectCount[4]!=0&& !add) {
                    current.trainCount[4]-=adder;
                    current.trainSelectCount[4]+=adder;
                }
                break;
                case BLACK:
                if (current.trainCount[5] != 0 && add) {
                    current.trainCount[5]-=adder;
                    current.trainSelectCount[5]+=adder;
                }
                else if(current.trainSelectCount[5]!=0&& !add) {
                    current.trainCount[5]-=adder;
                    current.trainSelectCount[5]+=adder;
                }
                break;
                case PINK:
                if (current.trainCount[6] != 0 && add) {
                    current.trainCount[6]-=adder;
                    current.trainSelectCount[6]+=adder;
                }
                else if(current.trainSelectCount[6]!=0 && !add) {
                    current.trainCount[6]=adder;
                    current.trainSelectCount[6]+=adder;
                }
                break;
                case YELLOW:
                if (current.trainCount[7] != 0 && add) {
                    current.trainCount[7]-=adder;
                    current.trainSelectCount[7]+=adder;
                }
                else if(current.trainSelectCount[7]!=0 && !add) {
                    current.trainCount[7]-=adder;
                    current.trainSelectCount[7]+=adder;
                }
                break;
                case LOCAMOTIVE:
                if (current.trainCount[8] != 0 && add) {
                    current.trainCount[8]-=adder;
                    current.trainSelectCount[8]+=adder;
                }
                else if(current.trainSelectCount[8]!=0&& !add) {
                    current.trainCount[8]-=adder;
                    current.trainSelectCount[8]+=adder;
                }
                break;
            }
        }
        catch (Exception e) {
            System.err.println("Error in LeftPanel Switch");
        }
    }

    int destPicCount=0;
    /**
     * This method is used to allow the user to interact with the 
     * left panel
     * @param e MouseEvent interactions with the mouse
     */
    public void mousePressed(MouseEvent e){
        int x = e.getX();
        int y = e.getY();
        if(e.getButton()==MouseEvent.BUTTON1){
            if (withinConvert(x,y)) {
                convertToLocamotive();
            }
            else if(25<=x&&x<=200){
                if (25<y&&y<=125) {
                    findWhichCard(true,0);
                }
                else if (125<y&&y<=225) {
                    findWhichCard(true,1);
                }
                else if (225<y&&y<=325) {
                    findWhichCard(true,2);
                }
                else if (325<y&&y<=425) {
                    findWhichCard(true,3);
                }
                else if (425<y&&y<=525) {
                    findWhichCard(true,4);
                }
                else if (525<=y&&y<=625) {
                    findWhichCard(true,5);
                }
                else if (625<=y&&y<=725) {
                    findWhichCard(true,6);
                }
                else if (725<=y&&y<=825) {
                    findWhichCard(true,7);
                }
                else if (825<y&&y<=950) {
                    findWhichCard(true,8);
                }
            }else if (withinTech(x,y)){
                //****************************//
                //VIEW PLAYER'S TECHNOLOGY CARDS
                //****************************//
                ArrayList<Technology> temp=current.getOwnedTech();
                if (temp.size() == 0) return;
                String[] full=new  String[temp.size()];
                for(int i=0; i<temp.size();i++){
                    full[i]=temp.get(i).getName();
                }
                JOptionPane.showInputDialog(null, 
                    "My Technologies", null, 
                    JOptionPane.PLAIN_MESSAGE,
                    null, full, full[0]);
                return;
            }else if (withinDest(x,y)){
                destPicCount++;
                if(destPicCount>=current.getDestinationCard()
                .size()){
                    destPicCount=0;
                }
                selectedDestPic=
                current.getDestinationCard().get(destPicCount)
                .getImage();
            }
        }else if(e.getButton()==MouseEvent.BUTTON3){
            if(25<=x&&x<=200){
                if (25<y&&y<=125) {
                    findWhichCard(false,0);
                }
                else if (125<y&&y<=225) {
                    findWhichCard(false,1);
                }
                else if (225<y&&y<=325) {
                    findWhichCard(false,2);
                }
                else if (325<y&&y<=425) {
                    findWhichCard(false,3);
                }
                else if (425<y&&y<=525) {
                    findWhichCard(false,4);
                }
                else if (525<=y&&y<=625) {
                    findWhichCard(false,5);
                }
                else if (625<=y&&y<=725) {
                    findWhichCard(false,6);
                }
                else if (725<=y&&y<=825) {
                    findWhichCard(false,7);
                }
                else if (825<y&&y<=950) {
                    findWhichCard(false,8);
                }
            }
        }
        repaint();
    }

    /**
     * This method is used to convert Trains to Locamotives
     */
    public void convertToLocamotive() {
        int offeredCards;
        boolean cont;
        if(current.getOwnedTech().contains(Technology.BOOSTER)){
            offeredCards = 3;
            cont = sufficientCards(3);
        }
        else {
            offeredCards = 4;
            cont = sufficientCards(4);
        }

        if (cont) {
            for (int i = 0; i < current.trainSelectCount.length - 1; i++) {
                if (current.trainSelectCount[i] != 0) {
                    while (current.trainSelectCount[i] != 0) {
                        offeredCards--;
                        current.trainSelectCount[i]--;
                        if (current.trainSelectCount[i] < 0) {
                            current.trainSelectCount[i] = 0;
                        }
                        if (current.trainCount[i] < 0) {
                            current.trainCount[i] = 0;
                        }
                        if (offeredCards == 0) {
                            current.trainCount[8]++;
                            repaint();
                            return;
                        }
                    }
                }
            }
        }
        else {
            error("You do not have enough cards to "+
                "perfrom a conversion");
        }
    }

    /**
     * This method is used to see if the player has
     * Sufficent cards for any particular task.
     * @param amt int need for the task
     */
    public boolean sufficientCards(int amt) {
        int sum = current.trainSelectCount[0] + 
            current.trainSelectCount[1] + 
            current.trainSelectCount[2] + 
            current.trainSelectCount[3] + 
            current.trainSelectCount[4] + 
            current.trainSelectCount[5] + 
            current.trainSelectCount[6]+ 
            current.trainSelectCount[7];
        if (sum >= amt) {
            return true;
        }
        return false;
    }

    /**
     * This method is used to set the default destination card
     * for a player
     * @param p Player 
     */
    public void setSelectedDestPic(Player p){
        selectedDestPic=p.getDestinationCard()
        .get(destPicCount).getImage();
    }

    /**
     * This method is used to determine if a click is within the
     * Convert button
     * @param x int coordinate
     * @param y int coordinate
     */
    public boolean withinConvert(int x, int y) {
        return (x > 210 && x < 340 && y > 300 && y < 427);
    }

    /**
     * This method is used to determine if a click is within the
     * Destination button
     * @param x int coordinate
     * @param y int coordinate
     */
    public boolean withinDest(int x, int y) {
        return(x > 210 && x < 340 && y > 475 && y < 649);
    }

    /**
     * This method is used to determine if a click is within the
     * Technology button
     * @param x int coordinate
     * @param y int coordinate
     */
    public boolean withinTech(int x, int y) {
        return (x > 210 && x < 340 && y > 700 && y < 800);
    }

    public void mouseClicked(MouseEvent e){}

    public void mouseEntered(MouseEvent e){}

    public void mouseReleased(MouseEvent e){}

    public void mouseExited(MouseEvent e){}

    /**
     * This method is used to tell the player the action they
     * commited is not allowed
     * @param s String the message to be told
     */
    public void error(String s){
        JOptionPane.showMessageDialog(this,s,"Sorry :(",
        JOptionPane.INFORMATION_MESSAGE);
    }

    Font display = new Font("Serif", Font.BOLD, 30);
    Font display2 = new Font("Serif", Font.BOLD, 22);
    int y = 25;
    /**
     * This method is used to paint the left panel
     * @param g Graphics the class used with painting
     */
    public void paintComponent(Graphics g)
    {
        g.drawImage(masterPic,0,0,getWidth(),getHeight(),this);
        g.setColor(current.getColor());
        g.setFont(display);
        g.drawString("Player: " + (current.getPlayerNumber()),
        220,72 );
        g.setFont(display2);
        g.drawString(current.getName(), 220,102 );
        g.setFont(display);
        g.drawString("Pieces: " + (current.getPiecesLeft() ), 
            220,132 );
        g.drawImage(current.getMasterPic(),210,150,130,130,this);
        g.drawImage(Driver.deck.getTrainImage(Train.LOCAMOTIVE),
            210,300,130,130,this);
        g.drawImage(techCard,210,700,130,100,this);
        g.drawImage(selectedDestPic,210,475,130,174,this);
        g.setFont(display2);
        g.setColor(Color.BLUE);
        g.drawString("Convert To:", 213,330);

        {//Hand Card Display
            g.drawImage(Driver.deck.getTrainImage(Train.RED),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[0]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[0]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.BLUE),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[1]+"",25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[1]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.GREEN),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[2]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[2]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.ORANGE),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[3]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[3]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.WHITE),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[4]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[4]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.BLACK),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[5]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[5]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.PINK),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[6]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[6]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(Train.YELLOW),
                25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[7]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[7]+"",
                170,y+20);
            y+=100;

            g.drawImage(Driver.deck.getTrainImage(
                    Train.LOCAMOTIVE),25,y,174,132,this);
            g.setFont(display);
            g.setColor(Color.BLACK);
            g.fillRect(25,y, 30,25 );
            g.setColor(current.getColor());
            g.drawString(current.trainCount[8]+"", 25,y+20);

            g.setColor(Color.BLACK);
            g.fillRect(170,y, 30,25 );
            g.setColor(Color.CYAN);
            g.drawString(current.trainSelectCount[8]+"",
                170,y+20);
            y+=100;

            y=25;
        }

    }
}
